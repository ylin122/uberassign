#include <iostream>
#include <fstream>
#include <cmath>
#include <unistd.h>

#include "lib/simulator.h"

bool check_bbox_collision(const sim::point_2d& location, const sim::points_2d& bbox)
{
	double buffer = 20;
    //        std::cout<<"checking location (x: "<<location.first<<", y: "<<location.second<<")"<<std::endl;
    bool xbound = location.first < bbox[0].first + buffer  || location.first > bbox[2].first -buffer;
    bool ybound = location.second< bbox[0].second + buffer || location.second> bbox[1].second -buffer;
    return xbound || ybound;
}

int main(void) {
    sim::simulator simulator;
    sim::points_2d bbox = simulator.get_bounding_box();
    simulator.initialize(10);

    std::ofstream ofs ("test.txt", std::ofstream::out);

    try
    {
        double control = 0;
        while(1){
            sim::simulation_state state(simulator.step(control));

            /***log obstacles position start***/
	    sim::object_map obj_map = state.object_states;
            for (auto it = obj_map.begin(); it != obj_map.end(); ++it)
	    {
                ofs << it->first << " " 
	            << it->second.first << " "
		    << it->second.second << "\n";
	    }
	    /***log obstacles position ends***/

            // This just dumbly applies full rate turn if we're about to run into a wall
            if(check_bbox_collision(state.player_state.location, bbox))
            {
                control = M_PI/6;
            }
            else
            {
                control = 0;
            }
            // This simply simulates the time a more complex planner might take
            usleep(16000);
        }
    } catch(const sim::simulator::simulation_halt_exception& e)
    {
        std::cout<<e.what()<<"\n"<<e.final_state;
    }

    return 0;
}
