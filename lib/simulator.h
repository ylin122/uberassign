/**
 * simulator.h
 * Copyright 2015
 *
 *
 * Uber Advanced Technology Center
 * Pittsburgh, PA
 *
 * Control Instructions (who can see this code):
 * Confidential.  Not for public release
 *
 *
 * This notice must appear in all copies of this file and its derivatives.
 */

#ifndef SIMULATOR_H_
#define SIMULATOR_H_

#include <vector>
#include <map>
#include <stdexcept>
#include <stdint.h>
#include <iostream>
#include <memory>

namespace sim
{

    /**
     * Represents a point in 2d space in the form x,y
     *
     */
    typedef std::pair<double, double> point_2d;

    /**
     * Represents a series of points in 2d space
     *
     */
    typedef std::vector<point_2d> points_2d;

    /**
     * Represents a Globally Unique Identifier
     */
    typedef int64_t guid;

    /**
     * A mapping between some uniquely identified object and its location
     */
    typedef std::map<guid, point_2d> object_map;

    /**
     * A structure which represents a state in 2D+Rotation State Space
     */
    struct state_SE2
    {
        /// the x,y component
        point_2d location;
        /// the angle component of the 2d state vector. Will be in range -pi to pi inclusive, in radians
        double theta;

        state_SE2();
        state_SE2(double x, double y, double theta);
        state_SE2(const point_2d& location, double theta);
        state_SE2(const state_SE2& copy);
        virtual ~state_SE2(){};

        state_SE2& operator=(state_SE2 rhs);

        /**
         * Adds  i :wtwo state_2d together element-wise, producing a new state_SE2
         */
        state_SE2 operator+(const state_SE2& rhs) const;

        /**
         * Subtracts two state_2d together element-wise, producing a new state_SE2
         */
        state_SE2 operator-(const state_SE2& rhs) const;

        /**
         * Multiplies a state_2d by a scaler element-wise, producing a new state_SE2
         */
        state_SE2 operator*(double scaler) const;

        friend std::ostream& operator<<(std::ostream& lhs, const state_SE2& rhs)
        {
            return lhs<<"(x: "<<rhs.location.first<<", y: "<<rhs.location.second<<", th: "<<rhs.theta<<")";
        }

    };

    /**
     * Overload of std::swap for state_SE2
     */
    void swap(state_SE2& lhs, state_SE2& rhs);


    /**
     * A structure which represents the current simulation state
     */
    struct simulation_state
    {
        /// The player's pose
        state_SE2 player_state;
        /// The current state of all objects in the world
        object_map     object_states;
        /// The number of seconds that have elapsed thus far in the simulation
        double         run_time;

        friend std::ostream& operator<<(std::ostream& lhs, const simulation_state& rhs)
        {
            lhs<<"Run Time: "<<rhs.run_time<<"\n"
                 "Player State: "<<rhs.player_state<<"\n"
                 "Objects: \n";
            for(const auto& object_data : rhs.object_states)
            {
                lhs<<object_data.first<<": (x: "<<object_data.second.first<<", y: "<<object_data.second.second<<")\n";
            }
            return lhs;
        }
    };

    class simulator_impl;

    class simulator
    {
    public:

        /**
         * Thrown when a collision has happened and the simulation ends.
         *
         * Contains the final state of the simulation at time of collision
         */
        class simulation_halt_exception : public std::runtime_error
        {
        public:
            simulation_halt_exception(const std::string& message, const simulation_state& state);
            const simulation_state final_state;
        private:
            std::string build_message(const std::string& message, const simulation_state& state);
        };

        simulator();

        virtual ~simulator();

        /**
         * Retrieves the vector of points representing the bounding box of the world
         * @return A points2d in the form {bottom_left, top_left, bottom_right, top_right}
         */
        points_2d get_bounding_box() const;

        /**
         * Initializes the simulation and starts the 'clock' running
         *
         * Once this method is called, the simulation is on.
         * @param number_of_objects The number of objects that will be in the simulation. Higher values means more difficulty
         * @return A simulation_state containing the initial state of the world
         * @throws runtime_error if called more than once on an initialized simulation
         */
        simulation_state initialize(int number_of_objects, bool use_graphics = true) throw(std::runtime_error);

        /**
         * Initializes the simulation with a user-supplied random seed. Useful for repeatable testing
         *
         * Note: We will *not* use a seed you provide us, so don't over-fit your solution to a particular seed!
         */
        simulation_state initialize(int number_of_objects, unsigned int rand_seed, bool use_graphics = true) throw(std::runtime_error);

        /**
         * Advances the state of the simulation and applies a new control input to the controlled vehicle.
         *
         * The time between this method being called is used to determine how far to advance the simulation.
         *
         * Note the steering angle supplied to this method is applied *after* advancing the world, not before. As with a real system, your
         * control inputs always lag behind real time.
         *
         * @param steering_angle The new steering angle to apply to the controlled vehicle.
         * @return The state of the simulation world after advancing the simulation by the time-interval since the last call to this method
         *
         * @throw simulation_halt_exception If the controlled vehicle collides with any object in the world, or with the walls at the edges.
         * @throw std::range_error If the supplied steering angle is outside what can be applied to the controlled vehicle
         * @throw std::runtime_error If this method is called before calling simulator::initialize
         */
        simulation_state step(double steering_angle) throw(simulation_halt_exception, std::range_error, std::runtime_error);

        /**
         * A debug version of step that allows specifying an amount of time that has elapsed since last step, rather than having it
         * derived from timing information. This is useful for debugging.
         */
        simulation_state step(double steering_angle, double elapsed_time) throw(simulation_halt_exception, std::range_error, std::runtime_error);

    private:
        std::unique_ptr<simulator_impl> impl;
    };

} /* namespace sim */

#endif /* SIMULATOR_H_ */
