#pragma once
namespace UserStructs{
  
  struct StateXV
  {
     //position
     double x;
     double y;
     //velocity
     double vx;
     double vy;

     StateXV();
     StateXV( double _x, double _y, double _vx, double _vy );

     ~StateXV(){ }

     //to predict the state after time t
     StateXV predict( double t );
  };
}
