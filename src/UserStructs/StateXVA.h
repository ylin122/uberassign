#pragma once
namespace UserStructs{

   struct StateXVA
   {
       //position
       double x;
       double y;
       //velocity
       double vx;
       double vy;
       //acceleration
       double ax;
       double ay;

       StateXVA();
       StateXVA( double _x, double _y, double _vx, double _vy, double _ax, double _ay );

       ~StateXVA(){ }

       //predict the state after time t
       StateXVA predict( double t );
   };

}
