#include "StateXV.h"

namespace UserStructs{
   
   StateXV::StateXV():x(0),y(0),vx(0),vy(0){

   }

   StateXV::StateXV( double _x, double _y, double _vx, double _vy ):x( _x ), y( _y ), vx( _vx ), vy( _vy ){

   }

   StateXV StateXV::predict(double t){
       return StateXV( x + vx * t, y + vy * t, vx, vy );
   }
	
}
