#include "StateXVA.h"

namespace UserStructs{

  StateXVA::StateXVA():x(0),y(0),vx(0),vy(0),ax(0),ay(0){

  }

  StateXVA::StateXVA( double _x, double _y, double _vx, double _vy, double _ax, double _ay ):
      x(_x),y(_y),vx(_vx),vy(_vy),ax(_ax),ay(_ay){

  }

  StateXVA StateXVA::predict( double t ){
      return StateXVA( x + vx * t + 0.5 * ax * t * t,
                       y + vy * t + 0.5 * ay * t * t,
                       x + vx * t,
                       y + vy * t,
                       ax,
                       ay );
  }

}
