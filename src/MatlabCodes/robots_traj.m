close all;
clear all;
clc;

f_traj = fopen('../../bin/robot.txt');

if f_traj == -1
    error('File test.txt could not be opened, check name or path.')
end

traj_line = fgetl(f_traj);
count = 0;
traj = [];

%trajectory
while ischar(traj_line)
   log_traj = textscan(traj_line,'%f %f %f %f'); 
   t = log_traj{1};
   x = log_traj{2};
   y = log_traj{3};
   hd = log_traj{4};
   
   if count == 0
      t0 = t;
      count = count + 1;
   end
   
   t = t - t0;
   
   traj = [ traj; [t,x,y,hd] ];
   traj_line = fgetl(f_traj);
end

velocity = [];
%speed
N = 60;
for i = N+1:size(traj,1)
    t = traj(i,1);
    dt = traj(i,1) - traj(i-N,1);
    vx = ( traj(i,2) - traj(i-N,2) ) / dt;
    vy = ( traj(i,3) - traj(i-N,3) ) / dt;
    speed = sqrt( vx*vx + vy*vy );
    v_hd = ( traj(i,4) - traj(i-N,4) ) / dt;
    velocity = [ velocity; [t, vx, vy, v_hd, speed] ];
end

M_vel = mean(velocity,1);
m_speed = M_vel(5);

var_vel = var(velocity,1);
var_speed = var_vel(5);

f_traj = fopen('../../bin/filter_robot.txt');

if f_traj == -1
    error('File filter_robot.txt could not be opened, check name or path.')
end

traj_line = fgetl(f_traj);
count = 0;
traj_filter = [];

%trajectory
while ischar(traj_line)
   log_traj = textscan(traj_line,'%f %f %f %f %f'); 
   t = log_traj{1};
   x = log_traj{2};
   y = log_traj{3};
   speed = log_traj{4};
   the = log_traj{5};
     
   if count == 0
      t0 = t;
      count = count + 1;
   end
   
   t = t - t0;
   
   traj_filter = [ traj_filter; [t,x,y,speed,the] ];
   traj_line = fgetl(f_traj);
end

figure;
axis auto;
grid on;
hold on;
title('position');
plot( traj(:,1), traj(:,2), 'k*' );
plot( traj_filter(:,1), traj_filter(:,2), 'm+' );

figure;
axis auto;
grid on;
hold on;
%ylim([-10,10]);
title('speed');
%plot( velocity(:,1), velocity(:,2), 'k*' );
%plot( velocity(:,1), velocity(:,3), 'r*' );
%plot( velocity(:,1), velocity(:,5), 'm*' );
plot( velocity(:,1), velocity(:,5), 'k*' );
plot( traj_filter(:,1), traj_filter(:,4), 'm+' );

%legend('vx','vy','speed','hd_rate');




