#pragma once

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/video/tracking.hpp"

#include <memory>

namespace Utils{
   //state (x,y,vx,vy,ax,ay)
   //measurement(x,y,vx,vy)
   class Kalman2{
      public:
       Kalman2( );

       ~Kalman2();

       void initialize( double x, double y, double vx, double vy, double ax, double ay );

       void SetDt( double _dt );

       void SetMeasure( double x_p, double y_p, double vx_p, double vy_p );

       void GetUpdate( double &x1, double &y1, double &vx1, double &vy1 );

       void GetUpdate( double &x1, double &y1, double &vx1, double &vy1, double &ax1, double &ay1 );

      private:
       //pointer to opencv's kalman filter
       std::unique_ptr< cv::KalmanFilter > filter;

       //measurement
       cv::Mat_< float > measurement;
   };
}

