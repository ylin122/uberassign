#include "Kalman.h"
#include "iostream"

namespace Utils{

    Kalman::Kalman( )
    {
        filter = std::unique_ptr< cv::KalmanFilter > ( new cv::KalmanFilter(4,2,0) );
        measurement = cv::Mat_< float >(2,1);
        measurement.setTo( cv::Scalar(0) );
    }

    Kalman::~Kalman()
    {

    }

    void Kalman::initialize( double x, double y, double vx, double vy )
    {
        //initialize the kalman filter
        filter->statePost.at< float >(0) = x;
        filter->statePost.at< float >(1) = y;
        filter->statePost.at< float >(2) = vx;
        filter->statePost.at< float >(3) = vy;

        cv::setIdentity( filter->measurementMatrix );
        cv::setIdentity( filter->processNoiseCov, cv::Scalar::all(1e-4) );
        cv::setIdentity( filter->measurementNoiseCov, cv::Scalar::all(1e-1) );
        cv::setIdentity( filter->errorCovPost, cv::Scalar::all(.1) );
    }

    void Kalman::SetDt( double _dt )
    {
        //set the transition matrix with dt
        filter->transitionMatrix = *( cv::Mat_< float >(4, 4) << 1,0,_dt,0,   0,1,0,_dt,  0,0,1,0,  0,0,0,1 );
    }

    void Kalman::SetMeasure(double x_p, double y_p)
    {
        //set measurement (x,y)
        measurement(0) = x_p;
        measurement(1) = y_p;

    }

    void Kalman::GetUpdate( double &x1, double &y1, double &vx1, double &vy1 )
    {
        //prediction
        filter->predict();
        //correction with measurement
        cv::Mat estimated = filter->correct( measurement );
        //get the updated state value
        x1 = estimated.at< float >(0);
        y1 = estimated.at< float >(1);
        vx1 = estimated.at< float >(2);
        vy1 = estimated.at< float >(3);
    }
}
