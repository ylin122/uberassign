#include "Kalman2.h"
#include "iostream"

namespace Utils{

   Kalman2::Kalman2( )
   {
       filter = std::unique_ptr< cv::KalmanFilter > ( new cv::KalmanFilter(6,4,0) );  
       measurement = cv::Mat_< float >(4,1);
       measurement.setTo( cv::Scalar(0) );
   }

   Kalman2::~Kalman2()
   {

   }

   void Kalman2::initialize( double x, double y, double vx, double vy, double ax, double ay )
   {
       //initialize the kalman filter
       filter->statePost.at< float >(0) = x;
       filter->statePost.at< float >(1) = y;
       filter->statePost.at< float >(2) = vx;
       filter->statePost.at< float >(3) = vy;
       filter->statePost.at< float >(4) = ax;
       filter->statePost.at< float >(5) = ay;

       cv::setIdentity( filter->measurementMatrix );
       cv::setIdentity( filter->processNoiseCov, cv::Scalar::all(1e-4) );
       cv::setIdentity( filter->measurementNoiseCov, cv::Scalar::all(1e-1) );
       cv::setIdentity( filter->errorCovPost, cv::Scalar::all(.1) );

   }

   void Kalman2::SetDt( double _dt )
   {
       //set the transition matrix with dt
       filter->transitionMatrix = *( cv::Mat_< float >(6, 6) <<
                                     1,0,_dt,0,0.5*_dt*_dt,0,
                                     0,1,0,_dt,0,0.5*_dt*_dt,
                                     0,0,1,0,_dt,0,
                                     0,0,0,1,0,_dt,
                                     0,0,0,0,1,0,
                                     0,0,0,0,0,1);
   }

   void Kalman2::SetMeasure(double x_p, double y_p, double vx_p, double vy_p )
   {
       //set measurement
       measurement(0) = x_p;
       measurement(1) = y_p;
       measurement(2) = vx_p;
       measurement(3) = vy_p;
   }

   void Kalman2::GetUpdate( double &x1, double &y1, double &vx1, double &vy1 )
   {
       //prediction
       filter->predict();
       //correction with measurement
       cv::Mat estimated = filter->correct( measurement );
       //get the updated state value
       x1 = estimated.at< float >(0);
       y1 = estimated.at< float >(1);
       vx1 = estimated.at< float >(2);
       vy1 = estimated.at< float >(3);
   }

   void Kalman2::GetUpdate( double &x1, double &y1, double &vx1, double &vy1, double &ax1, double &ay1 )
   {
       //prediction
       filter->predict();
       //correction with measurement
       cv::Mat estimated = filter->correct( measurement );

       x1 = estimated.at< float >(0);
       y1 = estimated.at< float >(1);
       vx1 = estimated.at< float >(2);
       vy1 = estimated.at< float >(3);
       ax1 = estimated.at< float >(4);
       ay1 = estimated.at< float >(5);
   }
}
