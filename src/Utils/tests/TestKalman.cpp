#include <gtest/gtest.h>
#include <memory>

#include "src/Utils/Kalman.h"

using namespace Utils;
using namespace cv;

TEST(Kalman,Kalman)
{
   Kalman filter;
   //= std::unique_ptr< Kalman >( new Kalman(4,2) );
   filter.initialize( 10, 10, 0, 0 );
   filter.SetDt( 1.0 );

   for( int i = 0; i != 40; ++i )
   {
       filter.SetMeasure( 10 + i, 10 + i );
       double  x, y, vx, vy;
       filter.GetUpdate( x, y, vx, vy );
       /*
       std::cout << "x:" << x << " " << "y:" << y << " "
                 << "vx:" << vx << " " << "vy:" << vy << '\n';
                 */
   }
}

TEST(Kalman, Original)
{
   //cv::KalmanFilter KF(4, 2, 0);
   auto KF = std::unique_ptr< KalmanFilter > ( new KalmanFilter(4, 2, 0) );
   Mat_< float > measurement;
   measurement = Mat_< float >(2,1);
   measurement.setTo(Scalar(0));

   double dt = 0.1;

   setIdentity(KF->measurementMatrix);
   setIdentity(KF->processNoiseCov, Scalar::all(1e-4));
   setIdentity(KF->measurementNoiseCov, Scalar::all(1e-1));
   setIdentity(KF->errorCovPost, Scalar::all(.1));

   KF->statePre.at< float >(0) = 10;
   KF->statePre.at< float >(1) = 10;
   KF->statePre.at< float >(2) = 0;
   KF->statePre.at< float >(3) = 0;
   KF->transitionMatrix = *(Mat_< float >(4, 4) << 1,0,dt,0,   0,1,0,dt,  0,0,1,0,  0,0,0,1);

   measurement(0) = 11;
   measurement(1) = 11;
   Mat prediction = KF->predict();
   Mat estimated = KF->correct(measurement);
}
