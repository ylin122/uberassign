#include "FindPath.h"
#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

namespace Utils{
 std::string FindPath()
 {
     std::string path1 = "/home/yucong/uberassign/";
     fs::path p1(path1);

     std::string path2 = "/home/yucong/UberCodes/mp_homework/";
     fs::path p2(path2);

     if( fs::exists(p1) ){
        return path1;
     }
     else{
         return path2;
     }
 }

}
