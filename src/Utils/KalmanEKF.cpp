#include "KalmanEKF.h"
#include <cmath>

namespace Utils {

   KalmanEKF::KalmanEKF( ) : L(4.8304)
   {
       filter = std::unique_ptr< cv::KalmanFilter > ( new cv::KalmanFilter(4,4,0) );     
       measurement = cv::Mat_< float >(4,1);
       measurement.setTo( cv::Scalar(0) );
   }

   KalmanEKF::~KalmanEKF()
   {

   }

   void KalmanEKF::initialize( double x, double y, double speed, double theta )
   {
       //initialize
       cv::setIdentity( filter->measurementMatrix );
       cv::setIdentity( filter->processNoiseCov, cv::Scalar::all(1e-4) );
       cv::setIdentity( filter->measurementNoiseCov, cv::Scalar::all(1e-1) );
       cv::setIdentity( filter->errorCovPost, cv::Scalar::all(.1) );

       filter->statePost.at< float >(0) = x;
       filter->statePost.at< float >(1) = y;
       filter->statePost.at< float >(2) = speed;
       filter->statePost.at< float >(3) = theta;
   }

   void KalmanEKF::SetMeasure( double x_p, double y_p, double speed_p, double theta_p )
   {
       //set the measurement
       measurement(0) = x_p;
       measurement(1) = y_p;
       measurement(2) = speed_p;
       measurement(3) = theta_p;
   }

   void KalmanEKF::GetUpdate( double &x1, double &y1, double &speed, double &theta )
   {
       //value from last step
       double x_pre = filter->statePost.at< float >(0);
       double y_pre = filter->statePost.at< float >(1);
       double speed_pre = filter->statePost.at< float >(2);
       double theta_pre = _wrap_pi( filter->statePost.at< float >(3) );

       //Jacobian of transfer function
       filter->transitionMatrix = *( cv::Mat_< float >(4,4) <<
                                     1,0,cos(theta_pre)*dt,-speed_pre*sin(theta_pre)*dt,
                                     0,1,sin(theta_pre)*dt,speed_pre*cos(theta_pre)*dt,
                                     0,0,1,0,
                                     0,0,tan(phi)/L*dt,1
                                   );

       //predict
       filter->predict();
       filter->statePre.at<float>(0) = x_pre + speed_pre * cos( theta_pre ) * dt;
       filter->statePre.at<float>(1) = y_pre + speed_pre * sin( theta_pre ) * dt;
       filter->statePre.at<float>(2) = speed_pre;
       filter->statePre.at<float>(3) = _wrap_pi( theta_pre + speed_pre / L * tan( phi ) * dt );

       //update with the measurement
       filter->correct( measurement );

       filter->temp5.at<float>(0) = measurement.at<float>(0) - filter->statePre.at<float>(0);
       filter->temp5.at<float>(1) = measurement.at<float>(1) - filter->statePre.at<float>(1);
       filter->temp5.at<float>(2) = measurement.at<float>(2) - filter->statePre.at<float>(2);
       filter->temp5.at<float>(3) = measurement.at<float>(3) - _wrap_pi( filter->statePre.at<float>(3) );

       //get the estimated state
       filter->statePost = filter->statePre + filter->gain * filter->temp5;

       x1 = filter->statePost.at< float >(0);
       y1 = filter->statePost.at< float >(1);
       speed = filter->statePost.at< float >(2);
       theta = _wrap_pi( filter->statePost.at< float >(3) );
   }

   float KalmanEKF::_wrap_pi(float bearing)
   {
       //function to constrain an angle to (-2*M_PI, 2*M_PI]
       int c = 0;

       while (bearing > M_PI && c < 30) {
           bearing -= 2*M_PI;
           c++;
       }

       c = 0;

       while (bearing <=  -M_PI && c < 30) {
           bearing += 2*M_PI;
           c++;
       }

       return bearing;
   }

}
