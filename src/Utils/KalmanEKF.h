#pragma once

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/video/tracking.hpp"

#include <memory>

namespace Utils{

   class KalmanEKF{
     //EKF to estimate the robot's state
     //state (x,y,speed,theta)
     //measurement(x,y,speed,theta)
     public:
       KalmanEKF(  );

       ~KalmanEKF( );

       void initialize( double x, double y, double speed, double theta );

       void SetMeasure( double x_p, double y_p, double speed_p, double theta_p );

       void GetUpdate( double &x1, double &y1, double &speed, double &theta );

       inline void SetL( const double _L ){ this-> L = _L; }
       inline void SetDt( double _dt ){ this -> dt = _dt; }
       inline void SetPhi( double _phi ){ this -> phi = _phi; }

     private:
       std::unique_ptr< cv::KalmanFilter > filter;

       //measurement
       cv::Mat_< float > measurement;

       //L in the car's kinematic model
       double L;

       //dt step
       double dt;

       //phi the steer angle
       double phi;

       //constrain the range of an angle to (-pi, pi)
       float _wrap_pi(float bearing);
   };

}
