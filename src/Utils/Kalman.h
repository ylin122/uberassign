#pragma once

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/video/tracking.hpp"

#include <memory>

namespace Utils{
   //kalman filter
   //state (x,y,vx,vy)
   //measurement(x,y)
   class Kalman{
      public:
       Kalman( );

       ~Kalman();

       void initialize( double x, double y, double vx, double vy );

       void SetDt( double _dt );

       void SetMeasure( double x_p, double y_p );

       void GetUpdate( double &x1, double &y1, double &vx1, double &vy1 );

      private:

       //pointer to opencv's kalman filter
       std::unique_ptr< cv::KalmanFilter > filter;

       //measurement
       cv::Mat_< float > measurement;
   };
}


