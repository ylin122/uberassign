#include <iostream>
#include <fstream>
#include <cmath>
#include <unistd.h>
#include <iomanip>
#include <memory>
#include <utility>

#include "lib/simulator.h"
#include "src/Utils/GetTimeNow.h"
#include "src/Utils/YcLogger.h"
#include "src/Utils/Kalman3.h"
#include "src/Utils/KalmanEKF.h"
#include "src/Planners/MultiSteerPlanner.h"
#include "src/UserStructs/StateXV.h"

bool check_bbox_collision(const sim::point_2d& location, const sim::points_2d& bbox)
{
    double buffer = 20;
    //        std::cout<<"checking location (x: "<<location.first<<", y: "<<location.second<<")"<<std::endl;
    bool xbound = location.first < bbox[0].first + buffer  || location.first > bbox[2].first -buffer;
    bool ybound = location.second< bbox[0].second + buffer || location.second> bbox[1].second -buffer;
    return xbound || ybound;
}

int main(int argc, char** argv)
{
    //Utils::LogConfigurator myconfigurator("log4cxx_YcSim.properties", "log for YcSim");

    sim::simulator simulator;
    sim::points_2d bbox = simulator.get_bounding_box();

    if( argc >= 2 ){
        simulator.initialize( atoi(argv[1]) );
    }
    else{
        simulator.initialize( 0 );
    }

    std::ofstream ofs ("robot.txt", std::ofstream::out);
    std::ofstream obsf("movers.txt", std::ofstream::out);
    std::ofstream f_speed ("speed.txt", std::ofstream::out);
    std::ofstream filter_f("filtered3.txt", std::ofstream::out);
    std::ofstream filter_rf("filter_robot.txt", std::ofstream::out);

    double t_pre;
    double t_last = 10.0;
    sim::state_SE2 state_now, state_pre;
    sim::object_map obj_map_pre;

    bool if_start = false;
    double count_cycle = 0;

    double L = 4.8304;
    double max_steer = M_PI / 6;
    double safe_r = 5.;
    double dt = 0.01, t1;

    auto planner = std::unique_ptr< planner::MultiSteerPlanner > ( new planner::MultiSteerPlanner( L, max_steer, safe_r ) );

    std::vector< std::unique_ptr< Utils::Kalman3 > > filters;
    auto robot_filter = std::unique_ptr< Utils::KalmanEKF >( new Utils::KalmanEKF() );
    robot_filter -> SetL( L );

    try
    {
        double control = 0;
        //double t_init = Utils::GetTimeNow();
        while(1){
            t1 = Utils::GetTimeNow();
            sim::simulation_state state(simulator.step(control));
            /*
            if( count_cycle == 0 ){
                state_pre = state.player_state;
            }*/

            /***log obstacles position start***/
            sim::object_map obj_map = state.object_states;

            for (auto it = obj_map.begin(); it != obj_map.end(); ++it)
            {
                 obsf << std::setprecision(12) << Utils::GetTimeNow() << " "
                      << it->first << " "
                      << it->second.first << " "
                      << it->second.second << "\n";
            }

            if( count_cycle == 1 )
            {
                if( !if_start ){

                    t_pre = Utils::GetTimeNow();
                    //movers filter
                    for( int i = 0; i != obj_map.size(); ++i )
                    {
                        auto filter = std::unique_ptr< Utils::Kalman3 >( new Utils::Kalman3() );
                        double x_mover = obj_map[i].first;
                        double y_mover = obj_map[i].second;
                        double vx = x_mover - obj_map_pre[i].first;
                        double vy = y_mover - obj_map_pre[i].second;
                        vx /= dt;
                        vy /= dt;

                        std::cout << "mover:" << x_mover << ' ' << y_mover << '\n';
                        filter -> initialize( x_mover, y_mover, vx, vy, 0, 0 );
                        filters.push_back( std::move( filter )  );
                    }
                    //robot filter

                    state_now = state.player_state;
                    double vx = state_now.location.first - state_pre.location.first;
                    double vy = state_now.location.second - state_pre.location.second;
                    vx /= dt;
                    vy /= dt;
                    double speed = std::sqrt( vx*vx + vy*vy );

                    robot_filter -> initialize( state.player_state.location.first, state.player_state.location.second, speed, state.player_state.theta );

                    if_start = true;
                }
                else
                {
                    std::vector< UserStructs::StateXVA > obstacles;
                    for( int i = 0; i != filters.size(); ++i )
                    {
                        double x_mover = obj_map[i].first;
                        double y_mover = obj_map[i].second;
                        double vx = x_mover - obj_map_pre[i].first;
                        double vy = y_mover - obj_map_pre[i].second;
                        vx /= dt;
                        vy /= dt;

                        filters[i] -> SetMeasure( x_mover, y_mover, vx, vy );
                        filters[i] -> SetDt( dt );
                        double x, y, ax, ay;
                        filters[i] -> GetUpdate( x, y, vx, vy, ax, ay );

                        filter_f << std::setprecision(12) << Utils::GetTimeNow() << " "
                                 << i << " "
                                 << x << ' ' << y << ' '
                                 << vx << ' ' << vy << ' '
                                 << ax << ' ' << ay << '\n';
                        obstacles.push_back( UserStructs::StateXVA( x_mover, y_mover, vx, vy, ax, ay ) );
                    }

                    double x, y, speed, theta;

                    double vx = state.player_state.location.first - state_pre.location.first;
                    double vy = state.player_state.location.second - state_pre.location.second;
                    vx /= dt;
                    vy /= dt;
                    double m_speed = std::sqrt( vx*vx + vy*vy );

                    robot_filter -> SetMeasure( state.player_state.location.first, state.player_state.location.second, m_speed, state.player_state.theta );
                    robot_filter -> SetDt( dt );
                    robot_filter -> SetPhi( control );
                    robot_filter -> GetUpdate( x, y, speed, theta );

                    if( speed < 0){
                        speed = m_speed;
                    }

                    filter_rf << std::setprecision(12) << Utils::GetTimeNow() << " "
                              << x << " " << y << " "
                              << speed << " " << theta << '\n';
                    //t_last = 10 / speed;
                    control = planner -> GetSteerMove3( speed, state.player_state, obstacles, bbox, t_last );
                }
            }
            /***log robot states start***/

            ofs << std::setprecision(12) << Utils::GetTimeNow() << " "
                << state.player_state.location.first << " "
                << state.player_state.location.second << " "
                << state.player_state.theta << '\n';

            /***log robot states end***/
            /*
            std::cout << "state:" << speed << " "
                      << state.player_state.location.first << " "
                      << state.player_state.location.second << " "
                      << state.player_state.theta << '\n';
            */

            // This just dumbly applies full rate turn if we're about to run into a wall
            /*
            if(check_bbox_collision(state.player_state.location, bbox))
            {
                control = M_PI/6;
                //std::cout << "Reactive Wall Turn" << '\n';
            }
            else
            {
                control = 0;
            }*/

            /*
            if( speed != 0 ){
                control = planner -> GetSteerClose( speed, state.player_state, obj_map, bbox, t_last );
            }
            */

            // This simply simulates the time a more complex planner might take
            usleep(16000);
            if( count_cycle == 0 ){
                ++ count_cycle;

            }
            state_pre = state.player_state;
            obj_map_pre = obj_map;

            dt = Utils::GetTimeNow() - t1;
        }
    } catch(const sim::simulator::simulation_halt_exception& e)
    {
        std::cout<<e.what()<<"\n"<<e.final_state;
    }

    return 0;
}
