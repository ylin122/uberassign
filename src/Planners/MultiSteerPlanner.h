#pragma once
#include <memory>
#include "lib/simulator.h"
#include "src/UserStructs/StateXV.h"
#include "src/UserStructs/StateXVA.h"

namespace planner{
    class MultiSteer_impl;

    class MultiSteerPlanner{
      public:
        MultiSteerPlanner();
        MultiSteerPlanner(double _L, double _max_steer, double _safe_r);
        ~MultiSteerPlanner();

        //set L, the distance between the rear and front wheels’ axles
        //the model d(theta)/dt = u/L * tan(phi);
        //theta is the heading, u is the speed, phi is the steering angle of the front wheels.
        void SetL(const double _L);

        //set maximum steer angle
        void SetMaxSteerAngle(const double _max_steer);

        //get steering angle for control
        double GetSteer(const double _speed,
                        const sim::state_SE2& state,
                        const sim::object_map& obstacles,
                        const sim::points_2d& bbox,
                        const double t_last
                        ) const;

        //get steering angle when the robot is close to the wall or movers
        double GetSteerClose(const double _speed,
                             const sim::state_SE2& state,
                             const sim::object_map& obstacles,
                             const sim::points_2d& bbox,
                             const double t_last
                             ) const;

        //get steering angle to avoid moving obstacles, using the sum of distance to select steer control
        double GetSteerMove(const double _speed,
                            const sim::state_SE2& state,
                            const std::vector< UserStructs::StateXV >& obstacles,
                            const sim::points_2d& bbox,
                            const double t_last
                            ) const;

        //get steering angle to avoid moving obstacles, using the closest distance to select the steer control
        double GetSteerMove2(const double _speed,
                             const sim::state_SE2 &state,
                             const std::vector<UserStructs::StateXV> &obstacles,
                             const sim::points_2d &bbox,
                             const double t_last,
                             double pre_control
                             ) const;

        double GetSteerMove3(const double _speed,
                             const sim::state_SE2 &state,
                             const std::vector<UserStructs::StateXVA> &obstacles,
                             const sim::points_2d &bbox,
                             const double t_last
                             ) const;

        double GetSteerReactive(const double _speed,
                                const sim::state_SE2& state,
                                const std::vector< UserStructs::StateXV >& obstacles,
                                const sim::points_2d& bbox,
                                const double t_last,
                                double thres_r
                               ) const;

      private:
        std::unique_ptr< MultiSteer_impl > impl;
    };

}
