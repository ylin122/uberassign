#include "MultiSteerPlanner.h"
#include "MultiSteer_impl.h"
#include "lib/simulator.h"

namespace planner{

  MultiSteerPlanner::MultiSteerPlanner()
  {
      impl = std::unique_ptr< MultiSteer_impl >( new MultiSteer_impl() );
  }

  MultiSteerPlanner::MultiSteerPlanner(double _L, double _max_steer, double _safe_r)
  {
      impl = std::unique_ptr< MultiSteer_impl >( new MultiSteer_impl(_L, _max_steer, _safe_r) );
  }

  MultiSteerPlanner::~MultiSteerPlanner()
  {

  }

  void MultiSteerPlanner::SetL(const double _L)
  {
      impl->SetL(_L);
  }

  void MultiSteerPlanner::SetMaxSteerAngle(const double _max_steer)
  {
      impl->SetMaxSteerAngle(_max_steer);
  }

  double MultiSteerPlanner::GetSteerMove2(const double _speed,
                                          const sim::state_SE2& state,
                                          const std::vector< UserStructs::StateXV >& obstacles,
                                          const sim::points_2d& bbox,
                                          const double t_last,
                                          double pre_control
                                          ) const
  {
      return impl->GetSteerMove2( _speed, state, obstacles, bbox, t_last, pre_control );
  }

  double MultiSteerPlanner::GetSteerReactive(const double _speed,
                                             const sim::state_SE2& state,
                                             const std::vector< UserStructs::StateXV >& obstacles,
                                             const sim::points_2d& bbox,
                                             const double t_last,
                                             double thres_r
                                             ) const
  {
      return impl->GetSteerReactive( _speed, state, obstacles, bbox, t_last, thres_r );
  }

  double MultiSteerPlanner::GetSteerMove(const double _speed,
                                         const sim::state_SE2& state,
                                         const std::vector< UserStructs::StateXV >& obstacles,
                                         const sim::points_2d& bbox,
                                         const double t_last
                                         ) const
  {
      return impl->GetSteerMove( _speed, state, obstacles, bbox, t_last );
  }

  double MultiSteerPlanner::GetSteerMove3(const double _speed,
                                          const sim::state_SE2& state,
                                          const std::vector< UserStructs::StateXVA >& obstacles,
                                          const sim::points_2d& bbox,
                                          const double t_last
                                          ) const
  {
      return impl->GetSteerMove3( _speed, state, obstacles, bbox, t_last );
  }

}
