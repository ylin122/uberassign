#include "MultiSteer_impl.h"
#include "SteerColliChecker.h"
#include "src/Utils/YcLogger.h"

#include <algorithm>

//logger for functions in this class
namespace {
Utils::LoggerPtr s_logger(Utils::getLogger("UberAssign.MultiSteer_impl.YcLogger"));
}

namespace planner
{

MultiSteer_impl::MultiSteer_impl():max_steer( M_PI / 6 ),if_control( false )
{
    colli_checker = std::unique_ptr< SteerColliChecker > ( new SteerColliChecker() );
}

MultiSteer_impl::MultiSteer_impl( double _L, double _max_steer, double _safe_r ):max_steer( _max_steer ), if_control( false )
{
    colli_checker = std::unique_ptr< SteerColliChecker > ( new SteerColliChecker( _L, _max_steer, _safe_r ) );
}

MultiSteer_impl::~MultiSteer_impl()
{

}

void MultiSteer_impl::SetL(const double _L){
    colli_checker->SetL(_L);
}

void MultiSteer_impl::SetMaxSteerAngle(const double _max_steer)
{
    colli_checker->SetMaxSteerAngle(_max_steer);
    this-> max_steer = _max_steer;
}

double MultiSteer_impl::GetSteerMove2(const double _speed,
                                      const sim::state_SE2& state,
                                      const std::vector< UserStructs::StateXV >& obstacles,
                                      const sim::points_2d& bbox,
                                      const double t_last,
                                      double pre_control
                                      )
{
    //*****************IDEA**********************************************************//
    //the steer angle range [-max_steer, max_steer] are evenly divided in to N steer
    //angles. For each steer angle, we use the robot's kinematic model and the movers'
    //position and velocity to predict the robot's and movers' position in a limit time
    //horizon and check for their collision. Steer angles result in no collisioin will
    //assigned a weight. The weight is calculated as the largest of the robot's closest
    //distance to each mover and the wall box in the time horizon. The steer angle produces
    //the largest weight will be the generated steer angle command for the robot.

    // the steer angle range [-max_steer,max_steer] are evenly discretized.
    int N = 30;
    // the decrement
    double d_steer = 2 * max_steer / N;

    //the vector of the pairs: steer angle, weight
    std::vector< steer_weight > steer_options;

    for( int i = 0; i != N + 1; ++i )
    {
        //for each steer angle
        double steer = -max_steer + d_steer * i;

        //try to calculate the closest distance between the robot and any mover or the box  wall as the weight
        // -1 will be returned if the steer angle command will result in collision
        double dis = colli_checker -> SteerValidateMove2( steer, _speed, state, obstacles, bbox, t_last);

        //if no collision is predicted, the < steer, weight > pair will be pushed into the
        //vector
        if( dis != -1){
            steer_options.push_back( steer_weight( steer, dis ) );
        }
    }

    //check if 0 steer angle will result in collision
    double dis0 = colli_checker -> SteerValidateMove2( 0, _speed, state, obstacles, bbox, t_last );
    //if no collision is predicted, we also add 0 steer angle's weight pair.
    if( dis0 != -1 )
    {
        steer_options.push_back( steer_weight( 0., dis0 )  );
    }

    //if we have valid options for steer angles
    if( !steer_options.empty() )
    {
        if_control = true;

        //sort according to weigth
        std::sort ( steer_options.begin(), steer_options.end(), SortFunc2 );

        std::cout << "multi steer planner output:" << steer_options[0].first * 180 / M_PI << '\n';
        YCLOG(s_logger,LL_DEBUG, "yes steer:" << steer_options[0].first * 180 / M_PI );

        //choose the one with the largest weight
        return steer_options[0].first;
    }
    else //if all steer angle options are predicted to result in collision
    {

        if( if_control ) {
            //print
            std::cout << "no steer options from the multi steer planner" << '\n';
            std::cout << "let's try the local planner." << '\n';
            /*
            std::cout << "speed:" << _speed << '\n';

            std::cout << state.location.first << ","
                      << state.location.second << ","
                      << state.theta << '\n';

            for (auto it = obstacles.begin(); it != obstacles.end(); ++it)
            {
                std::cout << '{' << it->x << "," << it->y << ","
                          << it->vx << "," << it->vy << '}' << ',' << '\n';
            }
            */
            if_control = false;
        }

        //first choice: just use the previous control angle
        //hopefully next time instance we can have valid steer angle options
        double control = pre_control;

        //if the robot is close to any mover or the wall, generate reactive steer control
        double x = state.location.first;
        double y = state.location.second;
        double thres_r = _speed * t_last;

        if( colli_checker -> BoxCloseDisWrap( x, y, bbox ) < thres_r
                || colli_checker -> CloseObssDisWrap( x, y, 0, obstacles ) < thres_r
                )
        {
            control = GetSteerReactive( _speed, state, obstacles, bbox, 3, thres_r );
            std::cout << "local planner control:" << control * 180 / M_PI << '\n';
            YCLOG( s_logger, LL_DEBUG, "reactive control2:" << control * 180 / M_PI );
        }

        return control;
    }

}

double MultiSteer_impl::GetSteerReactive(const double _speed,
                                         const sim::state_SE2& state,
                                         const std::vector< UserStructs::StateXV >& obstacles,
                                         const sim::points_2d& bbox,
                                         const double t_last,
                                         double thres_r
                                         )
{
    /******************** the backup reactive strategy to generate steer angle control **
     */
    /***************************IDEA*******************************************/
    //here, we still divide the steer angle range [-max_steer, max_steer] evenly into N
    //steer angles. However, for each steer angle, we no more check for collisions between
    //the robot and movers or the wall box in time horizon. We only calculate the closest
    //distance between the robot and movers or the wall box in time horizon, and use the
    //distance as the weight. We select the steer angle with the largest weight as the
    //generated steer angle. Here we use a shorter time horizon.

    int N = 30;
    double d_steer = 2 * max_steer / N;

    //the pair: steer, closeness to box and movers
    std::vector< steer_weight > steer_options;

    //log the obstacles whose distance smaller thres_r
    for( int i = 0; i != obstacles.size(); ++i )
    {
        double dis = colli_checker -> ObsSingleCloseDisWrap( state.location.first, state.location.second, 0., obstacles[i] );

        if(  dis < thres_r )
        {
            YCLOG( s_logger, LL_DEBUG, "obs:" << i << ' '
                   << obstacles[i].x << ' ' << obstacles[i].y << ' '
                   << obstacles[i].vx << ' ' << obstacles[i].vy
                   );
        }

    }

    //log the robot's current speed
    YCLOG( s_logger, LL_DEBUG, "speed:" << _speed );

    //dis_sum is used to judge if all the weights are zero
    double dis_sum = 0;


    for( int i = 0; i != N + 1; ++i )
    {
        double steer = -max_steer + d_steer * i;

        //try to calculate the closest distance between the robot and any mover or the box  wall as the weight. Collision check is not performed.
        double dis = colli_checker -> SteerCloseMove( steer, _speed, state, obstacles, bbox, t_last);

        dis_sum += dis;

        YCLOG( s_logger, LL_DEBUG, "reactive selection:" << ' '
               << "steer:"<< steer * 180./M_PI << ' '
               << "dis:" << dis );

        //push the < steer, weight > pair into the vector
        steer_options.push_back( steer_weight( steer, dis ) );
    }

    //push the < 0, weight > pair into the vector.
    double dis0 = colli_checker -> SteerCloseMove( 0, _speed, state, obstacles, bbox, t_last );
    steer_options.push_back( steer_weight( 0., dis0 )  );

    //sort the < steer, weight > pair vectors.
    std::sort ( steer_options.begin(), steer_options.end(), SortFunc2 );
    //the control steer angle is the one with the largest weight
    double control = steer_options[0].first;

    //if all steer angles' weight are zeros
    if( dis_sum == 0 ){
        //see which in [ -M_PI/6, M_PI/6 ] will result in larger closes distance
        double dis_pos30 = colli_checker -> SteerCloseMove2( M_PI / 6, _speed, state, obstacles, bbox, t_last );
        double dis_neg30 = colli_checker -> SteerCloseMove2( -M_PI / 6, _speed, state, obstacles, bbox, t_last );
        control = dis_pos30 > dis_neg30 ? M_PI / 6 : -M_PI / 6;
    }

    return control;
}

/**********the followings are functions not used in the final version************/

double MultiSteer_impl::GetSteer(const double _speed,
                                 const sim::state_SE2 &state,
                                 const sim::object_map &obstacles,
                                 const sim::points_2d &bbox,
                                 const double t_last
                                 )
{
    /***In this function, we treat movers as static obstacle in each time instance
    and check if each steer angle will result in collision with the static obstacles.
    We select the smallest steer angle that result in no collision as the generated angle.
    ****/

    int N = 30;
    double d_steer = 2 * max_steer / N;
    std::vector< double > steer_options;
    for( int i = 0; i != N + 1; ++i )
    {
        double steer = -max_steer + d_steer * i;
        if( i == N ){
            //std::cout << "extreme streer:" << steer * 180. / M_PI << '\n';
        }
        bool colli = colli_checker -> SteerValidate( steer, _speed, state, obstacles, bbox, t_last);
        if( !colli ){
            steer_options.push_back( steer );
            //std::cout << "steer good:" << steer / M_PI * 180. << '\n';
        }
    }

    if( !colli_checker -> SteerValidate( 0, _speed, state, obstacles, bbox, t_last ) )
    {
        steer_options.push_back( 0.0 );
    }

    if( !steer_options.empty() ){
        //sort according to absolute value of steer angle
        std::sort ( steer_options.begin(), steer_options.end(), SortFunc );
        int idx = steer_options.size();
        //std::cout << "idx:" << idx << '\n';
        return steer_options[ idx ];
    }
    else{
        std::cout << "no steer options" << '\n';

        std::cout << _speed << " "
                  << state.location.first << ","
                  << state.location.second << ","
                  << state.theta << '\n';

        for (auto it = obstacles.begin(); it != obstacles.end(); ++it)
        {

            std::cout << it->first << " "
                      << it->second.first << " "
                      << it->second.second << "\n";
        }
        /*
              std::cout << "-max:" << colli_checker -> SteerValidate( -M_PI / 6, _speed, state, obstacles, bbox, t_last ) << '\n';
              std::cout << "max:" << colli_checker -> SteerValidate( M_PI / 6, _speed, state, obstacles, bbox, t_last ) << '\n';
              */
        return 0;
    }

}

double MultiSteer_impl::GetSteerClose(const double _speed,
                                      const sim::state_SE2& state,
                                      const sim::object_map& obstacles,
                                      const sim::points_2d& bbox,
                                      const double t_last
                                      )
{
    /***In this function, we treat movers as static obstacle in each time instance
    and check if each steer angle will result in collision with the static obstacles.
    The difference from GetSteer function is that it only deal with movers close to the
    robot. We select the smallest steer angle that result in no collision as the generated angle.
    ****/

    bool if_close = colli_checker -> CheckClose( state, obstacles, bbox );
    double control = 0;
    if( if_close )
    {
        int N = 30;
        double d_steer = 2 * max_steer / N;
        //std::vector< double > steer_options;
        for( int i = 0; i != N + 1; ++i )
        {
            double steer = -max_steer + d_steer * i;
            bool colli = colli_checker -> SteerValidate( steer, _speed, state, obstacles, bbox, t_last);
            if( !colli ){
                //steer_options.push_back( steer );
                control = steer;
                break;
            }
        }
        if( control != 0 ){
            std::cout << "yes, close control:"
                      << control * 180. / M_PI << '\n';
            if_control = true;
        }
        else{
            if( if_control ){
                std::cout << "no, close control" << '\n';

                std::cout << "speed:" << _speed << '\n';

                std::cout << "state:" << " "
                          << state.location.first << ","
                          << state.location.second << ","
                          << state.theta << '\n';

                for (auto it = obstacles.begin(); it != obstacles.end(); ++it)
                {
                    std::cout << '{'
                              << it->first << ","
                              << it->second.first << ","
                              << it->second.second
                              << '}' << "," << "\n";
                }

                if_control = false;
            }
        }
    }
    return control;
}

double MultiSteer_impl::GetSteerMove(const double _speed,
                                     const sim::state_SE2& state,
                                     const std::vector< UserStructs::StateXV >& obstacles,
                                     const sim::points_2d& bbox,
                                     const double t_last
                                     )
{
    /****This function is similar to GetSteerMove2 function. The difference is that here we
     * use the sum of the distance to all movers and the box wall as the weight
     * ***/

    int N = 30;
    double d_steer = 2 * max_steer / N;
    //the pair: steer, closeness to box and movers
    std::vector< steer_weight > steer_options;

    for( int i = 0; i != N + 1; ++i )
    {
        double steer = -max_steer + d_steer * i;
        double dis = colli_checker -> SteerValidateMove( steer, _speed, state, obstacles, bbox, t_last);
        if( dis != -1){
            steer_options.push_back( steer_weight( steer, dis ) );
        }
    }

    double dis0 = colli_checker -> SteerValidateMove( 0, _speed, state, obstacles, bbox, t_last );
    if( dis0 != -1 )
    {
        steer_options.push_back( steer_weight( 0., dis0 )  );
    }

    if( !steer_options.empty() )
    {
        if_control = true;
        //sort according to absolute value of steer angle
        std::sort ( steer_options.begin(), steer_options.end(), SortFunc2 );

        std::cout << "yes steer:" << steer_options[0].first * 180 / M_PI << '\n';

        return steer_options[0].first;
    }
    else
    {
        if( if_control ) {
            std::cout << "no steer options" << '\n';

            std::cout << "speed:" << _speed << '\n';

            std::cout << state.location.first << ","
                      << state.location.second << ","
                      << state.theta << '\n';

            for (auto it = obstacles.begin(); it != obstacles.end(); ++it)
            {
                std::cout << '{' << it->x << "," << it->y << ","
                          << it->vx << "," << it->vy << '}' << ',' << '\n';
            }
            if_control = false;
        }

        return 0;
    }
}

double MultiSteer_impl::GetSteerMove3(const double _speed,
                                      const sim::state_SE2& state,
                                      const std::vector< UserStructs::StateXVA >& obstacles,
                                      const sim::points_2d& bbox,
                                      const double t_last
                                      )
{
    /****This function is similar to GetSteerMove2 function. The difference is that here we
     * use ( position, velocity, acceleration ) as the mover's state, and this such state
     * to predict the movers' motion and check for collision with the robot.
     * ***/

    int N = 30;
    double d_steer = 2 * max_steer / N;
    //the pair: steer, closeness to box and movers
    std::vector< steer_weight > steer_options;

    for( int i = 0; i != N + 1; ++i )
    {
        double steer = -max_steer + d_steer * i;
        double dis = colli_checker -> SteerValidateMove3( steer, _speed, state, obstacles, bbox, t_last);
        if( dis != -1){
            steer_options.push_back( steer_weight( steer, dis ) );
        }
    }

    double dis0 = colli_checker -> SteerValidateMove3( 0, _speed, state, obstacles, bbox, t_last );
    if( dis0 != -1 )
    {
        steer_options.push_back( steer_weight( 0., dis0 )  );
    }

    if( !steer_options.empty() )
    {
        if_control = true;
        //sort according to absolute value of steer angle
        std::sort ( steer_options.begin(), steer_options.end(), SortFunc2 );

        std::cout << "yes steer:" << steer_options[0].first * 180 / M_PI << '\n';

        return steer_options[0].first;
    }
    else
    {
        if( if_control ) {
            std::cout << "no steer options" << '\n';

            std::cout << "speed:" << _speed << '\n';

            std::cout << state.location.first << ","
                      << state.location.second << ","
                      << state.theta << '\n';

            for (auto it = obstacles.begin(); it != obstacles.end(); ++it)
            {
                std::cout << '{' << it->x << "," << it->y << ","
                          << it->vx << "," << it->vy << '}' << ',' << '\n';
            }
            if_control = false;
        }

        return 0;
    }

}

/******Get Steer Angles reactively from potential functions *********/

double MultiSteer_impl::GetSteerPotential( const sim::state_SE2& state,
                                           const std::vector< UserStructs::StateXV >& obstacles,
                                           const sim::points_2d& bbox
                                           )
{
    double x_rob = state.location.first;
    double y_rob = state.location.second;
    double the_rob = state.theta;

    std::pair< double, double > force( PotentialBox( x_rob, y_rob, bbox ).first + PotentialMovers( x_rob, y_rob, obstacles ).first, PotentialBox( x_rob, y_rob, bbox ).second + PotentialMovers( x_rob, y_rob, obstacles ).second );

    double f_theta = std::atan2( force.second, force.first );
    double d_phi = f_theta - the_rob;
    return d_phi ? M_PI / 6 : -M_PI / 6;
}

/****the followings are potential functions to generate reactive avoidance steer angle
 * control****/

std::pair< double, double > MultiSteer_impl::PotentialBox( double x, double y, const sim::points_2d& bbox )
{
    double big_f = 1e5;
    double k = 1.0;
    double f_left, f_right, f_up, f_down;

    double left = x - bbox[0].first;
    double right = bbox[2].first - x;
    double up = bbox[1].second - y;
    double down = y - bbox[0].second;

    f_left = left <= 2.5 ? big_f : k / left;
    f_right = right <= 2.5 ? -big_f : -k / right;
    f_up = up <= 2.5 ? -big_f : -k / up;
    f_down = down <= 2.5 ? big_f : k / down;

    return std::pair< double, double >( f_left + f_right, f_up + f_down );

}

std::pair< double, double > MultiSteer_impl::PotentialMovers( double x, double y, const std::vector< UserStructs::StateXV >& obstacles )
{
    std::pair< double, double > forces;
    double f_x = 0;
    double f_y = 0;

    for( int i = 0; i != obstacles.size(); ++i ){
        auto f = PotentialMover( x, y, obstacles[i] );
        f_x += f.first;
        f_x += f.second;
    }
    return std::pair< double, double >( f_x, f_y );
}

std::pair< double, double > MultiSteer_impl::PotentialMover( double x, double y, UserStructs::StateXV obstacle )
{
    double dis = std::sqrt( std::pow( x - obstacle.x, 2 ) + std::pow( y - obstacle.y, 2 )    );
    double k = 1.0;

    return std::pair< double, double >( k * obstacle.vx / dis, k * obstacle.vy / dis );
}

}
