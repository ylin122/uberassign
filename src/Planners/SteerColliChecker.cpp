#include "SteerColliChecker.h"
#include "lib/simulator.h"
#include "src/Utils/YcLogger.h"

#include <cmath>
#include <limits>

namespace {
   Utils::LoggerPtr s_logger(Utils::getLogger("UberAssign.SteerColliChecker.YcLogger"));
}

namespace planner
{
   SteerColliChecker::SteerColliChecker()
   {

   }

   SteerColliChecker::~SteerColliChecker()
   {

   }

   SteerColliChecker::SteerColliChecker(double _L, double _max_steer, double _safe_r):L(_L),MaxSteer(_max_steer),safe_r(_safe_r)
   {

   }

   /**********the following functions are used in final version************/

   double SteerColliChecker::SteerValidateMove2(const double phi,
                                                const double _speed,
                                                const sim::state_SE2 &state,
                                                const std::vector<UserStructs::StateXV> &obstacles,
                                                const sim::points_2d &bbox,
                                                const double t_last
                                                )
   {
       /***function to check if the steer control phi will lead to collision with movers or the wall box. It return -1 if a collision is predicted, otherwise it returns the cloest distance to any mover or box wall during the time horizon*****/
       double res = std::numeric_limits< double >::max();//the return value
       double dt = 0.1;//time propagation step to predict the robot's motion
       double t = 0.;//the start tiime
       //(x,y,the) is the robot's initial configuration.
       double x = state.location.first;
       double y = state.location.second;
       double the = state.theta;

       while( t <= t_last )
       {
           //propagate the robot's kinematic model (as in (13.15) of http://planning.cs.uiuc.edu/node658.html) stepwise
           double d_the = _speed / L * tan(phi) * dt;
           the += d_the;

           double dx = dt * _speed * cos( the );
           double dy = dt * _speed * sin( the );
           x += dx;
           y += dy;

           t += dt;

           //check collision and distance to the box walls
           //return -1 if collision occurs
           //otherwise return the distance to the closest wall
           double dis_box = CheckBoxPointDis2( x, y, bbox, safe_r );
           if( dis_box == -1){
               res = -1;
               break;
           }

           //check collision and distance to the movers
           //return -1 if collision occurs
           //otherwise return the distance to the closest mover
           double dis_obss = CheckObss2( x, y, t, obstacles );
           if( dis_obss == -1 ){
               res = -1;
               break;
           }

           //return the closest distance between the distance to the mover
           //and the wall
           res = std::min( res, std::min( dis_obss, dis_box ) );
       }

       return res;

   }

   double SteerColliChecker::SteerCloseMove(const double phi,
                                            const double _speed,
                                            const sim::state_SE2 &state,
                                            const std::vector<UserStructs::StateXV> &obstacles,
                                            const sim::points_2d &bbox,
                                            const double t_last
                                            )
   {
       /*the function is similar to SteerValidateMove2, but we just calculate
        * the closest distance to the movers or the wall without checking for collisons
        * here: if the robot run into the wall and go to negative distance, the
        * distance is assigned as 0.
       */
       double dt = 0.1;
       double t = 0.;
       double x = state.location.first;
       double y = state.location.second;
       double the = state.theta;

       double dis_box = std::numeric_limits< double >::max();
       double dis_obss = std::numeric_limits< double >::max();

       while( t <= t_last )
       {
           double d_the = _speed / L * tan(phi) * dt;
           the += d_the;

           double dx = dt * _speed * cos( the );
           double dy = dt * _speed * sin( the );
           x += dx;
           y += dy;

           t += dt;

           dis_box = std::min( BoxCloseDis( x, y, bbox ), dis_box );
           dis_obss = std::min( CloseObssDis( x, y, t, obstacles ), dis_obss );
       }

       return std::min( dis_box, dis_obss );
   }

   double SteerColliChecker::SteerCloseMove2(const double phi,
                                             const double _speed,
                                             const sim::state_SE2 &state,
                                             const std::vector<UserStructs::StateXV> &obstacles,
                                             const sim::points_2d &bbox,
                                             const double t_last
                                             )
   {
       /*the function is also similar to SteerValidateMove2, but we just calculate
        * the closest distance to the movers or the wall without checking for collisons
        * here: if the robot run into the wall and go to negative distance, the
        * distance is assigned to the negative distance.
       */
       double dt = 0.1;
       double t = 0.;
       double x = state.location.first;
       double y = state.location.second;
       double the = state.theta;

       double dis_box = std::numeric_limits< double >::max();
       double dis_obss = std::numeric_limits< double >::max();

       while( t <= t_last )
       {
           double d_the = _speed / L * tan(phi) * dt;
           the += d_the;

           double dx = dt * _speed * cos( the );
           double dy = dt * _speed * sin( the );
           x += dx;
           y += dy;

           t += dt;

           dis_box = std::min( BoxCloseDis2( x, y, bbox ), dis_box );
           dis_obss = std::min( CloseObssDis( x, y, t, obstacles ), dis_obss );
       }

       return std::min( dis_box, dis_obss );
   }

   double SteerColliChecker::BoxCloseDis(double x, double y, const sim::points_2d &bbox)
   {
       //to calculate the distance between an arbitary point (x,y) to the cloest wall
       //if (x,y) goes beyond the wall box area, the distance is assigned to 0
       double left = x - bbox[0].first;
       double right = bbox[2].first - x;
       double up = bbox[1].second - y;
       double down = y - bbox[0].second;

       if( left < 0 || right < 0 || up < 0 || down < 0 )
       {
           return 0;
       }
       else{
           return std::min( std::min( left, right ), std::min( up, down ) );
       }
   }

   double SteerColliChecker::BoxCloseDisWrap( double x, double y, const sim::points_2d& bbox )
   {
       //wrapper for BoxCloseDis function
       return BoxCloseDis( x, y, bbox );
   }

   double SteerColliChecker::BoxCloseDis2( double x, double y, const sim::points_2d &bbox )
   {
       //to calculate the distance between an arbitary point (x,y) to the cloest wall
       //if (x,y) goes beyond the wall box area, the distance is assigned to the original
       //negative value
       double left = x - bbox[0].first;
       double right = bbox[2].first - x;
       double up = bbox[1].second - y;
       double down = y - bbox[0].second;

       return std::min( std::min( left, right ), std::min( up, down ) );
   }

   double SteerColliChecker::CheckBoxPointDis2(double x, double y, const sim::points_2d &bbox, double buffer)
   {
       //check for collision between the point (x,y) and wall box.
       //buffer is the allowed distance between the point and the cloest wall
       //return -1 if collision detected, otherwise return the cloest distance

       double left = x - bbox[0].first;
       double right = bbox[2].first - x;
       double up = bbox[1].second - y;
       double down = y - bbox[0].second;

       if( left > buffer && right > buffer
         && up > buffer && down > buffer)
       {
           return std::min( std::min( left, right ), std::min( up, down ) );
       }
       else{
           return -1;
       }
   }

   double SteerColliChecker::CheckObss2(double x, double y, double t, std::vector<UserStructs::StateXV> obstacles)
   {
       //check for collision between the point (x,y) and all the movers.
       //return -1 if collision detected, otherwise return the distance
       //to the closest mover.

       double res = 0;
       double temp = std::numeric_limits< double >::max();
       for( int i = 0; i != obstacles.size(); ++i )
       {
           double dis = CheckObsSingle( x, y, t, obstacles[i] );
           if( dis == -1 ){
               res = -1;
               break;
           }
           else{
               temp = std::min( temp, dis );
           }
       }
       if( res != -1 ){
           res = temp;
       }
       return res;
   }

   double SteerColliChecker::CheckObsSingle(double x, double y, double t, UserStructs::StateXV obstacle)
   {
       //return the distance between point (x,y) and the predicted mover's position
       //at time t later. return -1 if the distance is less than safe_r.
       UserStructs::StateXV obs = obstacle.predict(t);
       double res = std::sqrt( std::pow( x - obs.x, 2 ) + std::pow( y - obs.y, 2 ) );
       if( res < safe_r ){
           res = -1;
       }

       return res;
   }

   bool SteerColliChecker::CheckBoxPoint( double x, double y, const sim::points_2d& bbox, double buffer )
   {
       //check if point (x,y) stays more than buffer distance to any wall
       bool xbound = x < bbox[0].first + buffer  || x > bbox[2].first -buffer;
       bool ybound = y < bbox[0].second + buffer || y > bbox[1].second -buffer;
       return xbound || ybound;
   }

   bool SteerColliChecker::CheckBoxPointWrap(double x, double y, const sim::points_2d &bbox, double buffer)
   {
       //wrapper for CheckBoxPoint function
       return CheckBoxPoint(x, y, bbox, buffer);
   }

   double SteerColliChecker::ObsSingleCloseDis(double x, double y, double t, UserStructs::StateXV obstacle)
   {
       //return the distance between point (x,y) and the predicted mover's position
       //at time t later.
       UserStructs::StateXV obs = obstacle.predict(t);
       double res = std::sqrt( std::pow( x - obs.x, 2 ) + std::pow( y - obs.y, 2 ) );
       return res;
   }

   double SteerColliChecker::ObsSingleCloseDisWrap( double x, double y, double t, UserStructs::StateXV obstacle )
   {
       //wrapper of function ObsSingleCloseDis
       return ObsSingleCloseDis( x, y, t, obstacle );
   }

   double SteerColliChecker::CloseObssDis(double x, double y, double t, std::vector<UserStructs::StateXV> obstacles)
   {
       //return the closest distance between point (x,y) and
       //the predicted positions of all the movers at time t later.
       double temp = std::numeric_limits< double >::max();
       for( int i = 0; i != obstacles.size(); ++i )
       {
           double dis = ObsSingleCloseDis( x, y, t, obstacles[i] );
           temp = std::min( temp, dis );

       }
       return temp;
   }

   double SteerColliChecker::CloseObssDisWrap( double x, double y, double t, std::vector< UserStructs::StateXV > obstacles )
   {
       //wrapper of function CloseObssDis
       return CloseObssDis( x, y, t, obstacles );
   }



   /********************************used functions ends*******************************/

   /************the following functions are developed in trial phase but not used in the final version**********************/

   bool SteerColliChecker::SteerValidate(const double phi,
                                         const double speed,
                                         const sim::state_SE2 &state,
                                         const sim::object_map &obstacles,
                                         const sim::points_2d &bbox,
                                         const double t_last
                                         )
   {
       /***function to check if the steer control phi will lead to collision with movers or the wall box. It return true if a collision is predicted, otherwise return false. However, it only treats the movers as static obstacles and use geometry to check collisions with obstacles and walls*****/
       //(x,y,theta) of the robot
       double x_robot = state.location.first;
       double y_robot = state.location.second;
       double theta_robot = state.theta;

       if( phi != 0 )
       {
           //the turning rate of the robot
           double omega = speed / L * tan( phi );
           //the turning radian
           double R = std::abs( L / tan( phi ) );

           //the angle of vector (x,y) --> turning circle center W.R.T to the x axis
           double ang_c = omega > 0 ? theta_robot + M_PI / 2 : theta_robot - M_PI / 2;
           //center of the turning circle
           double x_c = x_robot + R * cos( ang_c );
           double y_c = y_robot + R * sin( ang_c );

           //check collision with box
           bool res_box = CheckBox( x_c, y_c, bbox, R );

           bool res_movers = false;
           if( ! res_box ){
             //check collision with movers
             res_movers = CheckMovers( x_c, y_c, x_robot, y_robot, obstacles, omega, t_last, R );
           }

           return res_movers || res_box;
       }
       else
       {
           double x_end = x_robot + speed * cos(theta_robot) * t_last;
           double y_end = y_robot + speed * sin(theta_robot) * t_last;
           bool res_box = CheckBoxPoint( x_end, y_end, bbox, 2 * safe_r );
           bool res_movers = false;
           if( !res_box ){
               res_movers = CheckMoversLine( x_robot, y_robot, x_end, y_end, obstacles );
           }

           return res_box || res_movers;
       }

   }

   double SteerColliChecker::SteerValidateMove(const double phi,
                                               const double _speed,
                                               const sim::state_SE2 &state,
                                               const std::vector<UserStructs::StateXV> &obstacles,
                                               const sim::points_2d &bbox,
                                               const double t_last
                                               )
   {
       /***function to check if the steer control phi will lead to collision with movers or the wall box. It return -1 if a collision is predicted, otherwise it returns the summed distance to all the movers and the box wall during the time horizon*****/

       double res = 0;
       double dt = 0.1;
       double t = 0.;
       double x = state.location.first;
       double y = state.location.second;
       double the = state.theta;
       //the weight between the distance to the box and distance to movers
       double weight = 0.5;

       while( t <= t_last )
       {
           double d_the = _speed / L * tan(phi) * dt;
           the += d_the;

           double dx = dt * _speed * cos( the );
           double dy = dt * _speed * sin( the );
           x += dx;
           y += dy;

           t += dt;

           //check collision and distance to the box walls
           double dis_box = CheckBoxPointDis( x, y, bbox, safe_r );
           if( dis_box == -1){
               res = -1;
               //std::cout << "box colli" << '\n';
               break;
           }

           double dis_obss = CheckObss( x, y, t, obstacles );
           if( dis_obss == -1 ){
               res = -1;
               break;
           }

           res += weight * dis_box + ( 1 - weight ) * dis_obss;
       }

       return res;
   }



   double SteerColliChecker::SteerValidateMove3(const double phi,
                                                const double _speed,
                                                const sim::state_SE2 &state,
                                                const std::vector<UserStructs::StateXVA> &obstacles,
                                                const sim::points_2d &bbox,
                                                const double t_last
                                                )
   {
       /***function to check if the steer control phi will lead to collision with movers or the wall box. It return -1 if a collision is predicted, otherwise it returns the closest distance to any mover or box wall during the time horizon. It uses ( position, velocity, accelaration ) to represent the movers' states in their motion prediction*****/
       double res = std::numeric_limits< double >::max();
       double dt = 0.1;
       double t = 0.;
       double x = state.location.first;
       double y = state.location.second;
       double the = state.theta;

       while( t <= t_last )
       {
           double d_the = _speed / L * tan(phi) * dt;
           the += d_the;

           double dx = dt * _speed * cos( the );
           double dy = dt * _speed * sin( the );
           x += dx;
           y += dy;

           t += dt;

           //check collision and distance to the box walls
           double dis_box = CheckBoxPointDis2( x, y, bbox, safe_r );
           if( dis_box == -1){
               res = -1;
               //std::cout << "box colli" << '\n';
               break;
           }

           double dis_obss = CheckObss3( x, y, t, obstacles );
           if( dis_obss == -1 ){
               res = -1;
               break;
           }

           res = std::min( res, std::min( dis_obss, dis_box ) );
       }

       return res;
   }



   bool SteerColliChecker::CheckClose( const sim::state_SE2 &state, const sim::object_map &obj_map, const sim::points_2d &bbox )
   {
       //function to check if the robot is close to the movers or the box wall
       double x_rob = state.location.first;
       double y_rob = state.location.second;

       bool box_close = CheckBoxPoint( x_rob, y_rob, bbox, 20 );
       bool movers_close = CheckMoversClose( x_rob, y_rob, obj_map, 20 );

       return box_close || movers_close ;
   }

   bool SteerColliChecker::CheckSingleMoverWrap(double x_c,
                                                double y_c,
                                                double x_rob,
                                                double y_rob,
                                                const sim::point_2d &mover_posi,
                                                double omega,
                                                double t_last,
                                                double R
                                                )
   {
       //wrapper of function CheckSingleMover
       return CheckSingleMover( x_c, y_c, x_rob, y_rob, mover_posi, omega, t_last, R );
   }

   bool SteerColliChecker::CheckSingleMoverLineWrap(double x_start,
                                                    double y_start,
                                                    double x_end,
                                                    double y_end,
                                                    const sim::point_2d &mover_posi
                                                    )
   {
       //wrapper of function CheckSingleMoverLine
       return CheckSingleMoverLine( x_start, y_start, x_end, y_end, mover_posi );
   }

   bool SteerColliChecker::CheckBoxWrap(double x_c, double y_c, const sim::points_2d &bbox, double R)
   {
       //wrapper of function CheckBox
       return CheckBox( x_c, y_c, bbox, R );
   }  

   bool SteerColliChecker::CheckSingleMover(double x_c,
                                            double y_c,
                                            double x_rob,
                                            double y_rob,
                                            const sim::point_2d& mover_posi,
                                            double omega,
                                            double t_last,
                                            double R
                                            )
   {
       //check if the mover whose position is mover_posi is in the fan area cencered at (x_c,y_c)
       double x_mover = mover_posi.first;
       double y_mover = mover_posi.second;

       //the angle between line1: center-->mover and line2: center-->robot
       double ang = LineAngle( x_c, y_c, x_mover, y_mover, x_rob, y_rob );
       //the distance between the mover and (x_c,y_c);
       double dis = std::sqrt( std::pow( x_c - x_mover, 2 ) + std::pow( y_c - y_mover, 2 ) );
       return ang < std::abs(1. * omega * t_last) && dis < R + 2 * safe_r;
   }

   bool SteerColliChecker::CheckMovers(double x_c,
                                       double y_c,
                                       double x_rob,
                                       double y_rob,
                                       const sim::object_map& obj_map,
                                       double omega,
                                       double t_last,
                                       double R
                                       )
   {
       //check if any mover is in the fan area cencered at (x_c,y_c)
       bool res = false;
       for( auto it = obj_map.begin(); it != obj_map.end(); ++it )
       {
           if( CheckSingleMover(x_c,y_c,x_rob,y_rob,it->second,omega,t_last,R) ){
               res = true;
               break;
           }
       }
       return res;
   }

   bool SteerColliChecker::CheckSingleMoverLine(double x_start,
                                                double y_start,
                                                double x_end,
                                                double y_end,
                                                const sim::point_2d &mover_posi
                                                )
   {
       //check if the mover whose position is mover_posi will intersect with the line (x_start,y_start) ---> (x_end,y_end)
       double x_mover = mover_posi.first;
       double y_mover = mover_posi.second;

       double ang_start = LineAngle( x_start, y_start, x_mover, y_mover, x_end, y_end );
       double ang_end = LineAngle( x_end, y_end, x_mover, y_mover, x_start, y_start );

       bool res_movers = false;

       if( ang_start < M_PI / 2 && ang_end < M_PI / 2)
       {// in this case, the mover is right above\below the line segment, we need to estimate the distance from (x_mover, y_mover) to the line itself
           double dis_start = std::sqrt( std::pow( x_mover - x_start, 2 ) + std::pow( y_mover - y_end, 2 ) );
           double dis = std::abs( dis_start * sin( ang_start ) );
           if( dis < 2 * safe_r ){
               res_movers = true;
           }
       }
       else if( ang_end >= M_PI / 2 )
       {// in this case, the mover is not right above\below the line segment, we need to estimate the distance from (x_mover, y_mover) to both ends of the line segment
           double dis_end = std::sqrt( std::pow( x_mover - x_end, 2 ) + std::pow( y_mover - y_end, 2 ) );
           if( dis_end < 2 * safe_r ){
               res_movers = true;
           }
       }
       else{

       }

       return res_movers;

   }

   bool SteerColliChecker::CheckMoversLine(double x_start,
                                           double y_start,
                                           double x_end,
                                           double y_end,
                                           const sim::object_map &obj_map
                                           )
   {
       //check if any mover will intersect with the line (x_start,y_start) ---> (x_end,y_end)
       bool res = false;
       for( auto it = obj_map.begin(); it != obj_map.end(); ++it )
       {
           if( CheckSingleMoverLine( x_start, y_start, x_end, y_end, it->second ) ){
               res = true;
               break;
           }
       }
       return res;
   }

   bool SteerColliChecker::CheckBox(double x_c, double y_c, const sim::points_2d& bbox, double R)
   {
       //check if a circle centered at (x_c,y_c) with redian R will intersect with the box
       //wall
       bool res = false;
       if( x_c + R + safe_r > bbox[2].first
               || x_c - R - safe_r < bbox[0].first
               || y_c + R + safe_r > bbox[1].second
               || y_c - R - safe_r < bbox[0].second
               )
       {
           res = true;
       }

       return res;
   }

   bool SteerColliChecker::CheckBoxBuffer(double x_c, double y_c, const sim::points_2d &bbox, double R, double buffer)
   {
       //check if a circle centered at (x_c,y_c) with redian R will intersect with the box
       //wall. Here a buffer is added.
       bool res = false;
       if( x_c + R + safe_r + buffer > bbox[2].first
               || x_c - R - safe_r - buffer < bbox[0].first
               || y_c + R + safe_r + buffer > bbox[1].second
               || y_c - R - safe_r - buffer < bbox[0].second
               )
       {
           res = true;
       }

       return res;
   }

   double SteerColliChecker::CheckBoxPointDis(double x, double y, const sim::points_2d &bbox, double buffer)
   {
       //check if the point (x,y) stays outside buffer from the wall
       //if no, return -1.
       //if yes ,return the sum of the distance to all four walls
       double left = x - bbox[0].first;
       double right = bbox[2].first - x;
       double up = bbox[1].second - y;
       double down = y - bbox[0].second;

       if( left > buffer && right > buffer
         && up > buffer && down > buffer)
       {
           return left + right + up + down;
       }
       else{
           return -1;
       }

   }

   bool SteerColliChecker::CheckMoversClose(double x, double y, const sim::object_map &obj_map, double thres )
   {
       //check if point (x,y) is safe_r + thres away from any mover.
       bool res = false;
       for( auto it = obj_map.begin(); it != obj_map.end(); ++it )
       {
           double x_mover = it->second.first;
           double y_mover = it->second.second;
           double dis = std::sqrt( std::pow( x - x_mover, 2 ) + std::pow( y - y_mover, 2 ) );
           if( dis < thres + safe_r ){
               res = true;
               break;
           }
       }
       return res;
   }

   double SteerColliChecker::CheckObss(double x, double y, double t, std::vector<UserStructs::StateXV> obstacles)
   {
       //check if the point (x,y) stays safe_r away from any mover
       //if no, return -1.
       //if yes ,return the sum of distances to all the movers
       double res = 0;
       for( int i = 0; i != obstacles.size(); ++i )
       {
           double dis = CheckObsSingle( x, y, t, obstacles[i] );
           if( dis == -1 ){
               res = -1;
               break;
           }
           else{
               res += dis;
           }
       }
       return res;
   }

   double SteerColliChecker::CheckObss3(double x, double y, double t, std::vector<UserStructs::StateXVA> obstacles)
   {
       //check for collision between the point (x,y) and all the movers.
       //return -1 if collision detected, otherwise return the distance
       //to the closest mover.
       //similar to function CheckObss2, but we use (position, velocity, acceleration)
       //to predict the movers' motion.
       double res = 0;
       double temp = std::numeric_limits< double >::max();
       for( int i = 0; i != obstacles.size(); ++i )
       {
           double dis = CheckObsSingle2( x, y, t, obstacles[i] );
           if( dis == -1 ){
               res = -1;
               break;
           }
           else{
               temp = std::min( temp, dis );
           }
       }
       if( res != -1 ){
           res = temp;
       }
       return res;
   }

   double SteerColliChecker::CheckObsSingle2(double x, double y, double t, UserStructs::StateXVA obstacle)
   {
       //return the distance between point (x,y) and the predicted mover's position
       //at time t later. return -1 if the distance is less than safe_r.
       //similar to function CheckObsSingle, but we use (position, velocity, acceleration)
       //to predict the movers' motion.
       UserStructs::StateXVA obs = obstacle.predict( t );

       double res = std::sqrt( std::pow( x - obs.x, 2 ) + std::pow( y - obs.y, 2 ) );
       if( res < safe_r ){
           res = -1;
       }

       return res;
   }

   double SteerColliChecker::Dis_2d(double x, double y, const sim::point_2d& point){
       //the distance between the point (x,y) and point
       return std::sqrt( (x-point.first)*(x-point.first) + (y-point.second)*(y-point.second) );
   }

   double SteerColliChecker::LineAngle( double x0, double y0, double x1, double y1, double x2, double y2 )
   {
       //the angle between line (x0,y0)-->(x1,y1) and line (x0,y0)-->(x2,y2)
       double xx1 = x1 - x0;
       double yy1 = y1 - y0;
       double xx2 = x2 - x0;
       double yy2 = y2 - y0;
       double dot12 = xx1 * xx2 + yy1 * yy2;
       double dot11 = std::sqrt( xx1 * xx1 + yy1 * yy1 );
       double dot22 = std::sqrt( xx2 * xx2 + yy2 * yy2 );
       //exception handle maybe needed.
       double ang = acos( dot12 / ( dot11 * dot22 ) );

       return ang;
   }
   /************************trial functions end*****************************/

}
