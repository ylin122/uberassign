#include <gtest/gtest.h>
#include <cmath>
#include <memory>
#include "src/Planners/MultiSteerPlanner.h"
#include "lib/simulator.h"

using namespace planner;

TEST(MultiSteerPlanner, GetSteerMoveTest)
{
    double L = 4.8304;
    double t_last = 10.;

    double speed = 2.34915;
    sim::state_SE2 state( 25.2206,18.3831,-2.71118 );

    double obs_array[][4] = {
        {68.977,5.21376,7.95317,0.605126},
        {37.9007,38.2602,-3.25237,-1.0723},
        {59.7933,40.3545,-3.82499,0.0167648},
        {90.1521,2.17813,0.52434,-4.51714},
        {75.0747,11.0412,2.31919,6.09687}
    };

    std::vector< UserStructs::StateXV > obstacles;

    for( int i = 0 ; i != 5; ++i ){
        obstacles.push_back( UserStructs::StateXV( obs_array[i][0], obs_array[i][1],  obs_array[i][2], obs_array[i][3] ) );
    }

    std::vector< std::pair<double,double> > bbox;
    bbox.push_back( std::pair< double, double >(0,0) );
    bbox.push_back( std::pair< double, double >(0,100) );
    bbox.push_back( std::pair< double, double >(100,0) );
    bbox.push_back( std::pair< double, double >(100,100) );

    double safe_r = 5.0;

    auto planner = std::unique_ptr< planner::MultiSteerPlanner > ( new planner::MultiSteerPlanner( L, M_PI/6, safe_r ) );

    double control2 = planner -> GetSteerMove2( speed, state, obstacles, bbox, t_last, 0);
    std::cout << "control2:" << control2 << '\n';
}

TEST( MultiSteerPlanner, GetSteerReactive )
{
    double L = 4.8304;

    double speed = 3.22067;
    double t_last = 3;

    sim::state_SE2 state( 20.8415,21.6681,1.60305 );

    double obs_array[][4] = {

        {4.33356,16.3873,-2.01943,0.247448},
        {6.39979,17.527,-5.43092,-0.901107},
        {25.8823,85.3522,-3.60424,1.94751},
        {71.0548,17.0458,5.16551,2.74056},
        {13.2887,39.2798,3.30963,-3.21085},
        {93.3854,62.0306,-2.84067,-1.2496},
        {38.5548,78.295,1.85482,0.705572},
        {86.3362,24.9599,4.52706,3.66586},
        {37.5401,16.856,-4.08295,-1.98803},
        {80.4584,59.9184,-7.12431,3.33932}
    };

    std::vector< UserStructs::StateXV > obstacles;
    int size = sizeof( obs_array )/sizeof( obs_array[0] );

    for( int i = 0; i != size; ++i ){
        obstacles.push_back( UserStructs::StateXV( obs_array[i][0], obs_array[i][1],  obs_array[i][2], obs_array[i][3] ) );
    }

    std::vector< std::pair<double,double> > bbox;
    bbox.push_back( std::pair< double, double >(0,0) );
    bbox.push_back( std::pair< double, double >(0,100) );
    bbox.push_back( std::pair< double, double >(100,0) );
    bbox.push_back( std::pair< double, double >(100,100) );

    double safe_r = 5.0;
    double thres_r = 15.;

    auto planner = std::unique_ptr< planner::MultiSteerPlanner > ( new planner::MultiSteerPlanner( L, M_PI/6, safe_r ) );

    double control = planner -> GetSteerReactive( speed, state, obstacles, bbox, t_last, thres_r );

    std::cout << "reactive control:" << control * 180./M_PI << '\n';

}

