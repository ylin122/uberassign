#include <gtest/gtest.h>
#include <cmath>
#include <memory>
#include "src/Planners/SteerColliChecker.h"
#include "lib/simulator.h"

using namespace planner;

TEST(ColliChecker, CheckMover)
{
    //class
    auto checker = std::unique_ptr< SteerColliChecker >( new SteerColliChecker() );

    //parameters
    double x_c = 0;
    double y_c = 0;
    double x_rob = 2;
    double y_rob = -2;
    double safe_r = 0.5;

    checker -> SetSafeRadian( safe_r );
    std::pair < double, double > mover( 2, 2 );
    double omega = M_PI / 12;
    double t_last = 3;
    double R = std::sqrt( std::pow( x_c - x_rob, 2 ) + std::pow( y_c - y_rob, 2 ) );

    bool res = checker -> CheckSingleMoverWrap( x_c, y_c, x_rob, y_rob, mover, omega, t_last, R );
    EXPECT_EQ( false, res );

    mover.second = -1;
    res = checker -> CheckSingleMoverWrap( x_c, y_c, x_rob, y_rob, mover, omega, t_last, R );
    EXPECT_EQ( true, res );

    mover.first = 2;
    double dis = std::sqrt( std::pow( x_c - mover.first, 2 ) + std::pow( y_c - mover.second, 2 ) );
    res = checker -> CheckSingleMoverWrap( x_c, y_c, x_rob, y_rob, mover, omega, t_last, R );
    EXPECT_EQ( res, dis < R + 2 * safe_r );

}

TEST(ColliChecker, CheckMoverLine)
{
    //class
    auto checker = std::unique_ptr< SteerColliChecker >( new SteerColliChecker() );

    //parameters
    double x_start = 0;
    double y_start = 0;
    double x_end = 2.0;
    double y_end = 0.;
    double safe_r = 0.5;

    checker -> SetSafeRadian( safe_r );
    std::pair < double, double > mover( 0.5, 0.5 );

    bool res = checker -> CheckSingleMoverLineWrap( x_start, y_start, x_end, y_end, mover );
    EXPECT_EQ( true, res );
}

TEST(ColliChecker, CheckBox)
{
    std::vector< std::pair<double,double> > bbox;
    bbox.push_back( std::pair< double, double >(0,0) );
    bbox.push_back( std::pair< double, double >(0,100) );
    bbox.push_back( std::pair< double, double >(100,0) );
    bbox.push_back( std::pair< double, double >(100,100) );

    double x_c = 50;
    double y_c = 50;
    double R = 15;
    double safe_r = 2.0;

    auto checker = std::unique_ptr< SteerColliChecker >( new SteerColliChecker() );
    checker -> SetSafeRadian( safe_r );
    bool res = checker -> CheckBoxWrap( x_c, y_c, bbox, R );

    EXPECT_EQ( false, res );
}

TEST(ColliChecker, CheckBoxPoint)
{
    std::vector< std::pair<double,double> > bbox;
    bbox.push_back( std::pair< double, double >(0,0) );
    bbox.push_back( std::pair< double, double >(0,100) );
    bbox.push_back( std::pair< double, double >(100,0) );
    bbox.push_back( std::pair< double, double >(100,100) );

    double x_c = 98;
    double y_c = 98;
    double safe_r = 5.0;

    auto checker = std::unique_ptr< SteerColliChecker >( new SteerColliChecker() );
    checker -> SetSafeRadian( safe_r );

    bool res = checker -> CheckBoxPointWrap( x_c, y_c, bbox, 2 * safe_r );

    EXPECT_EQ( true, res );
}

TEST(ColliChecker, SteerValidate)
{
    double phi = 30 / 180. * M_PI;
    double speed =  2.53362 ;
    sim::state_SE2 state( 86.1846,15.0278,-2.82345 );

    double obs_array[][3] = {
        {0,70.6207,9.98482},
        {1,44.2802,14.3654},
        {2,87.2856,86.1766},
        {3,33.1758,4.77514},
        {4,79.364,85.9084},
        {5,75.3919,71.9774},
        {6,45.0308,22.2958},
        {7,82.7656,81.463},
        {8,38.7847,83.0973},
        {9,25.4557,5.91444}
    };

    sim::object_map obj_map;
    for( int i = 0; i != 10; ++i )
    {
       obj_map[i] = std::pair< double, double > ( obs_array[i][1],  obs_array[i][2] );
    }

    std::vector< std::pair<double,double> > bbox;
    bbox.push_back( std::pair< double, double >(0,0) );
    bbox.push_back( std::pair< double, double >(0,100) );
    bbox.push_back( std::pair< double, double >(100,0) );
    bbox.push_back( std::pair< double, double >(100,100) );
    double t_last = 10;

    double safe_r = 5.0;
    double L = 4.8304;

    auto checker = std::unique_ptr< SteerColliChecker >( new SteerColliChecker() );
    checker -> SetSafeRadian( safe_r );
    checker -> SetL( L );

    bool res = checker -> SteerValidate( phi, speed, state, obj_map, bbox, t_last );

    EXPECT_EQ( true, res );
}

TEST(ColliChecker, SteerValidateMove)
{
    double phi = 30 / 180. * M_PI;
    double speed =  2 ;
    double L = 4.8304;
    double omega = speed / L * tan( phi );
    double ang = M_PI / 6;
    double t_last = ang / omega;
    double R = L / tan(phi);
    std::cout << "R:" << R << '\n';
    std::cout << "t_last:" << t_last << '\n';

    double x_rob = 50;
    double y_rob = 50;

    double x_end = x_rob + R * sin( ang );
    double y_end = y_rob + R * ( 1 - cos( ang ) );
    std::cout << "x_end:" << x_end << " "
              << "y_end:" << y_end << '\n';
    sim::state_SE2 state( x_rob, y_rob, 0 );

    std::vector< UserStructs::StateXV > obstacles;
    double x_mover = 70;
    double y_mover = 70;
    double vx = ( x_end - x_mover ) / t_last;
    double vy = ( y_end - y_mover ) / t_last;
    obstacles.push_back( UserStructs::StateXV( x_mover, y_mover, -vx, -vy ) );

    x_mover = 30;
    y_mover = 30;
    vx = ( x_end - x_mover ) / t_last;
    vy = ( y_end - y_mover ) / t_last;
    obstacles.push_back( UserStructs::StateXV( x_mover, y_mover, -vx, -vy ) );

    std::vector< std::pair<double,double> > bbox;
    bbox.push_back( std::pair< double, double >(0,0) );
    bbox.push_back( std::pair< double, double >(0,100) );
    bbox.push_back( std::pair< double, double >(100,0) );
    bbox.push_back( std::pair< double, double >(100,100) );

    double safe_r = 5.0;

    auto checker = std::unique_ptr< SteerColliChecker >( new SteerColliChecker() );
    checker -> SetSafeRadian( safe_r );
    checker -> SetL( L );

    double res = checker->SteerValidateMove( phi, speed, state, obstacles, bbox, t_last );

    EXPECT_EQ( res == -1, false );
}
