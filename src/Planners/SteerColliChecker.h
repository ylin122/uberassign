#pragma once
#include "lib/simulator.h"
#include "src/UserStructs/StateXV.h"
#include "src/UserStructs/StateXVA.h"

namespace planner
{
   //the class to check the collision between the robot and movers or the wall with different steer angle.
   class SteerColliChecker
   {
      public:
         //constructor
         SteerColliChecker();
         SteerColliChecker( double _L, double _max_steer, double _safe_r );

         //destructor
         ~SteerColliChecker();

         //set parameter L
         inline void SetL( const double _L ){this->L = _L;}

         //set the maximum steer angle
         inline void SetMaxSteerAngle( const double _max_steer ){this->MaxSteer = _max_steer;}

         //set the safety redian safe_r
         inline void SetSafeRadian( const double _r ){this->safe_r = _r;}

         /**********the following functions are used in final version************/

         double SteerValidateMove2(const double phi,
                                   const double _speed,
                                   const sim::state_SE2 &state,
                                   const std::vector< UserStructs::StateXV >& obstacles,
                                   const sim::points_2d &bbox,
                                   const double t_last
                                   );

         double SteerCloseMove( const double phi,
                                const double _speed,
                                const sim::state_SE2 &state,
                                const std::vector< UserStructs::StateXV >& obstacles,
                                const sim::points_2d &bbox,
                                const double t_last
                                );

         double SteerCloseMove2( const double phi,
                                 const double _speed,
                                 const sim::state_SE2 &state,
                                 const std::vector< UserStructs::StateXV >& obstacles,
                                 const sim::points_2d &bbox,
                                 const double t_last
                                 );

         double BoxCloseDisWrap( double x, double y, const sim::points_2d& bbox );

         bool CheckBoxPointWrap( double x, double y, const sim::points_2d& bbox, double buffer );

         double CloseObssDisWrap( double x, double y, double t, std::vector< UserStructs::StateXV > obstacles );

         double ObsSingleCloseDisWrap( double x, double y, double t, UserStructs::StateXV obstacle );

         /*****used functions ends***************************************/

         /************the following functions are developed in trial phase but not used in the final version**********************/

         bool SteerValidate(const double phi,
                            const double _speed,
                            const sim::state_SE2 &state,
                            const sim::object_map &obstacles,
                            const sim::points_2d &bbox,
                            const double t_last
                            );

         double SteerValidateMove(const double phi,
                                  const double _speed,
                                  const sim::state_SE2 &state,
                                  const std::vector< UserStructs::StateXV >& obstacles,
                                  const sim::points_2d &bbox,
                                  const double t_last
                                  );

         double SteerValidateMove3( const double phi,
                                    const double _speed,
                                    const sim::state_SE2 &state,
                                    const std::vector< UserStructs::StateXVA >& obstacles,
                                    const sim::points_2d &bbox,
                                    const double t_last
                                    );

         //check if the robot is close to wall or any movers
         bool CheckClose(const sim::state_SE2& state, const sim::object_map &obj_map, const sim::points_2d &bbox);

         //wrapper for private functions used in unit-test
         bool CheckSingleMoverWrap(double x_c,
                                   double y_c,
                                   double x_rob,
                                   double y_rob,
                                   const sim::point_2d& mover_posi,
                                   double omega,
                                   double t_last,
                                   double R
                                   );

         bool CheckBoxWrap( double x_c, double y_c, const sim::points_2d& bbox, double R );

         bool CheckSingleMoverLineWrap(double x_start,
                                       double y_start,
                                       double x_end,
                                       double y_end,
                                       const sim::point_2d& mover_posi
                                       );
         /************************trial functions end*****************************/

      private:

         //L, the distance between the rear and front wheels’ axles
         //the model d(theta)/dt = u/L * tan(phi);
         //theta is the heading, u is the speed, phi is the steering angle of the front wheels.
         double L;

         //the max steering angle of front wheels
         double MaxSteer;

         //the equivalent safe radian: including the size of both the robot and mover
         double safe_r;

      private:
         //functions

         /**********the following functions are used in final version************/

         //check if a point is in the box with buffer, return the distance to the closest wall
         double CheckBoxPointDis2( double x, double y, const sim::points_2d& bbox, double buffer = 0 );

         double BoxCloseDis( double x, double y, const sim::points_2d& bbox );

         double BoxCloseDis2( double x, double y, const sim::points_2d& bbox );

         //check if the robot will collides with obstacles at time t from zero time counting
         //return the distance to the closest obstacle
         double CheckObss2( double x, double y, double t, std::vector< UserStructs::StateXV > obstacles );

         double CloseObssDis( double x, double y, double t, std::vector< UserStructs::StateXV > obstacles );

         //check the potential collision with a single obstacle at time t, return the distance to it.
         double CheckObsSingle( double x, double y, double t, UserStructs::StateXV obstacle );

         double ObsSingleCloseDis( double x, double y, double t, UserStructs::StateXV obstacle );

         /***********************used function ends****************************/

         /************the following functions are developed in trial phase but not used in the final version**********************/

         //check the collision of potential trajectory with a single mover
         bool CheckSingleMover(double x_c,
                               double y_c,
                               double x_rob,
                               double y_rob,
                               const sim::point_2d& mover_posi,
                               double omega,
                               double t_last,
                               double R
                               );

         //check the collision of potential trajectory with all the movers in the environment
         bool CheckMovers(double x_c,
                          double y_c,
                          double x_rob,
                          double y_rob,
                          const sim::object_map& obstacles,
                          double omega,
                          double t_last,
                          double R
                          );

         //check the collision of a potential line with a single mover
         bool CheckSingleMoverLine(double x_start,
                                   double y_start,
                                   double x_end,
                                   double y_end,
                                   const sim::point_2d& mover_posi
                                   );

         //check the collision of a potential line with all the movers in the environment
         bool CheckMoversLine(double x_start,
                              double y_start,
                              double x_end,
                              double y_end,
                              const sim::object_map& obstacles
                              );

         //check the potential trajectory collision with the wall box
         bool CheckBox(double x_c, double y_c, const sim::points_2d& bbox, double R);

         //check the potential trajectory collision with the wall box with buffer
         bool CheckBoxBuffer(double x_c, double y_c, const sim::points_2d& bbox, double R, double buffer);

         //check the collision between a point and the wall box
         bool CheckBoxPoint( double x, double y, const sim::points_2d& bbox, double buffer = 0 );
         //check if a point is in the box with buffer, return the sum of the distances to all four walls
         double CheckBoxPointDis( double x, double y, const sim::points_2d& bbox, double buffer = 0 );



         //check the closeness between the robot and movers
         bool CheckMoversClose( double x, double y, const sim::object_map &obj_map, double thres );

         //check if the robot will collides with obstacles at time t from zero time counting
         //return the sum of distance to all the movers
         double CheckObss( double x, double y, double t, std::vector< UserStructs::StateXV > obstacles );       

         double CheckObss3( double x, double y, double t, std::vector< UserStructs::StateXVA > obstacles );

         double CheckObsSingle2( double x, double y, double t, UserStructs::StateXVA obstacle );

         //return the distance between (x,y) and a point_2d pair
         double Dis_2d(double x, double y, const sim::point_2d& point);

         //return the angle between line1 (x0,y0)-->(x1,y1), and line2 (x0,y0)-->(x2,y2)
         double LineAngle(double x0, double y0, double x1, double y1, double x2, double y2);
         /************************trial functions end*****************************/
   };
}

