#pragma once
#include <memory>
#include <cmath>
#include "lib/simulator.h"
#include "src/UserStructs/StateXV.h"
#include "src/UserStructs/StateXVA.h"

namespace planner
{
  // forward declaration. MultiSteer_impl owns a pointer of SteerColliChecker
  class SteerColliChecker;

  class MultiSteer_impl{
     public:
        //constructor
        MultiSteer_impl();
        MultiSteer_impl(double _L, double _max_steer, double _safe_r);
        //destructor
        ~MultiSteer_impl();

        //set L, the distance between the rear and front wheels’ axles
        //the model d(theta)/dt = u/L * tan(phi);
        //theta is the heading, u is the speed, phi is the steering angle of the front wheels.
        void SetL(const double _L);

        //set maximum steer angle
        void SetMaxSteerAngle(const double _max_steer);

        /**********the following two functions are used in final version************/

        //get steering angle to avoid moving obstacles, using the cloest distances to select
        double GetSteerMove2(const double _speed,
                             const sim::state_SE2& state,
                             const std::vector< UserStructs::StateXV >& obstacles,
                             const sim::points_2d& bbox,
                             const double t_last,
                             double pre_control
                             );

        //get steering angle to reactively avoid obstacles and wall
        double GetSteerReactive(const double _speed,
                                const sim::state_SE2& state,
                                const std::vector< UserStructs::StateXV >& obstacles,
                                const sim::points_2d& bbox,
                                const double t_last,
                                double thres_r
                                );

        /*********************final functions ends**************************/

        /****************the following five functions are trial functions developed previously, but not used in the final version*************/

        //get steering angle for control
        double GetSteer(const double _speed,
                        const sim::state_SE2& state,
                        const sim::object_map& obstacles,
                        const sim::points_2d& bbox,
                        const double t_last
                        );

        //get steering angle when the robot is close to the wall or movers
        double GetSteerClose(const double _speed,
                             const sim::state_SE2& state,
                             const sim::object_map& obstacles,
                             const sim::points_2d& bbox,
                             const double t_last
                             );

        //get steering angle to avoid moving obstacles, using the sum of distances to select
        double GetSteerMove(const double _speed,
                            const sim::state_SE2& state,
                            const std::vector< UserStructs::StateXV >& obstacles,
                            const sim::points_2d& bbox,
                            const double t_last
                            );

        //get reactive control using potential field methed
        double GetSteerPotential( const sim::state_SE2& state,
                                  const std::vector< UserStructs::StateXV >& obstacles,
                                  const sim::points_2d& bbox
                                  );

        //get steering angle to avoid moving obstacles, using the cloest distances to select
        double GetSteerMove3(const double _speed,
                             const sim::state_SE2& state,
                             const std::vector< UserStructs::StateXVA >& obstacles,
                             const sim::points_2d& bbox,
                             const double t_last
                             );
        /*****************trial functions ends*******************************/

     private:
        //typepdef < steer angle, weight for selection this angle > pair
        typedef std::pair< double, double > steer_weight;

        //the collision checker used in motion planning
        std::unique_ptr< SteerColliChecker > colli_checker;

        //the maxisum steer angle
        double max_steer;

        //the flag if an effective control is generated
        bool if_control;

        //funtions for sorting
        inline static bool SortFunc( double x, double y ) {
            return std::abs(x) < std::abs(y);
        }

        inline static bool SortFunc2( steer_weight sw1, steer_weight sw2 ){
            return sw1.second > sw2.second;
        }


        /************the following 3 functions are potential field to generate reactive
         * avoidance control, not used in the final version*****************/
        //function for potential field
        std::pair< double, double > PotentialBox( double x, double y, const sim::points_2d& bbox );

        //function for movers field
        std::pair< double, double > PotentialMovers( double x, double y, const std::vector< UserStructs::StateXV >& obstacles );

        std::pair< double, double > PotentialMover( double x, double y, UserStructs::StateXV obstacle );

  };
}

