#pragma once

#include "lib/simulator.h"
#include "src/Planners/MultiSteerPlanner.h"
#include "src/Utils/Kalman2.h"
#include "src/Utils/KalmanEKF.h"
#include <fstream>

namespace Main{
  //it is a wrapper for the main function. it is used to separate interface and implementation.
  class SimuMain{

    public:
      //constructor
      SimuMain( double _t_predict, double _L, double _max_steer, double _safe_r );
      //destructor
      ~SimuMain(){ }
      //the loop to generate avoidance steer control.
      void working();
      //set the number of movers
      inline void SetObsNum( int _obs_num ){ this->obs_num = _obs_num; }

    private:
      //the time horizon used to predict the robot's trajectory, here we predict its trajectory in 10 seconds forward
      double t_predict;

      //the parameter for the vehicle's kinematic model. L is the distance between the rear and front wheels' axles. please refer to http://planning.cs.uiuc.edu/node658.html.
      double L;

      //the absolute value for the robot's maximum steer angle.
      double max_steer;

      // the safety radius used in collision checking in motion planning. It is set 2 units larger than the robot's diameter to account for uncertainty.
      double safe_r;

      //the number of movers
      double obs_num;

      //the motion planner
      std::unique_ptr< planner::MultiSteerPlanner > planner;

      //the kalman fiters to estimate the velocities of the movers at each time instance
      std::vector< std::unique_ptr< Utils::Kalman2 > > filters;

      //the EKF to eastimate the speed of the robot at each time instance
      std::unique_ptr< Utils::KalmanEKF > robot_filter;

      //for data recording
      std::ofstream ofs;//to log the position of the robot
      std::ofstream obsf;//to log the position of the movers
      std::ofstream filter_f;//to log the filtered position and velocities of the movers
      std::ofstream filter_rf;//to log the filtered position, speed and heading of the robot.

      //to check if point (x,y) is in the box.
      bool check_bbox_collision(double x, double y, const sim::points_2d& bbox, double buffer );

  };

}
