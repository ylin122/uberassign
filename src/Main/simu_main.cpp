#include "src/Main/SimuMain.h"
#include "src/Utils/YcLogger.h"
#include <memory>
using namespace Main;

int main( int argc, char** argv )
{
    //load logger configuration to all the loggers used in any class or function called by this executable.
    Utils::LogConfigurator myconfigurator("log4cxx_YcSim.properties", "log for YcSim");

    //some basic parameters for the vehicle
    double t_predict = 10; //the time horizon used to predict the robot's trajectory, here we predict its trajectory in 10 seconds forward
    double L = 4.8304;//the parameter for the vehicle's kinematic model. L is the distance between the rear and front wheels' axles. please refer to http://planning.cs.uiuc.edu/node658.html.
    double max_steer = M_PI / 6.; //the absolute value for the robot's maximum steer angle.
    double safe_r = 7.0; // the safety radius used in collision checking in motion planning. It is set 2 units larger than the robot's diameter to account for uncertainty.

    //a smart pointer pointed to SimuMain object.
    auto simu_main = std::unique_ptr< SimuMain >( new SimuMain( t_predict, L, max_steer, safe_r ) );

    if( argc >= 2 ){
        //set the number of movers from user's input, the default value is 0
        simu_main -> SetObsNum( atoi(argv[1]) );
    }

    //the loop to repeatedly generate steering control to avoid movers.
    simu_main -> working();

    return 0;
}
