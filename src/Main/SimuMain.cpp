#include "SimuMain.h"

#include <iostream>
#include <fstream>
#include <cmath>
#include <unistd.h>
#include <iomanip>
#include <memory>
#include <utility>

#include "lib/simulator.h"
#include "src/Utils/GetTimeNow.h"
#include "src/Utils/YcLogger.h"
#include "src/Utils/Kalman2.h"
#include "src/Utils/KalmanEKF.h"
#include "src/Planners/MultiSteerPlanner.h"
#include "src/UserStructs/StateXV.h"

namespace Main{
  //constructor
  SimuMain::SimuMain(double _t_predict, double _L, double _max_steer, double _safe_r)
      : t_predict( _t_predict ),
        L( _L ),
        max_steer( _max_steer ),
        safe_r( _safe_r ),
        obs_num(0), ofs("../records/robot.txt"),
        obsf("../records/movers.txt"),
        filter_f("../records/filtered.txt"),
        filter_rf("../records/filter_robot.txt")
  {
      planner = std::unique_ptr< planner::MultiSteerPlanner > ( new planner::MultiSteerPlanner( L, max_steer, safe_r ) );
      robot_filter = std::unique_ptr< Utils::KalmanEKF >( new Utils::KalmanEKF() );
      robot_filter -> SetL( L );
  }

  void SimuMain::working()
  {
      //the simulator
      sim::simulator simulator;
      //the wall box
      sim::points_2d bbox = simulator.get_bounding_box();
      //to initialize movers
      simulator.initialize( obs_num );

      //the current state of the robot
      sim::state_SE2 state_now;
      //the state of the robot in the previous loop cycle
      sim::state_SE2 state_pre;
      //the movers position in the previous loop cycle
      sim::object_map obj_map_pre;

      //flag to indicate if the filtering procedure should start
      bool if_start = false;
      //to count the number of cycles
      double count_cycle = 0;
      //the time interval between nearby cycles, used in Kalman filters
      double dt;
      //a time point used to calculate dt;
      double t1;

      try
      {
          //the steer control
          double control = 0;
          //the execution loop;
          while(1){
              //the time when current cycle starts
              //we can use relevant functions in <chrono> library to replace my own time function.
              t1 = Utils::GetTimeNow();
              //step forward
              sim::simulation_state state(simulator.step(control));

              //get movers positions
              sim::object_map obj_map = state.object_states;

              /***log movers' position start***/
              for (auto it = obj_map.begin(); it != obj_map.end(); ++it)
              {
                   obsf << std::setprecision(12) << Utils::GetTimeNow() << " "
                        << it->first << " "
                        << it->second.first << " "
                        << it->second.second << "\n";
              }

              //do everything from the second cycle.
              if( count_cycle == 1 )
              {
                  if( !if_start ){
                      //kalman filters not initialized yet
                      //to initialize all the kalman filters

                      //movers' kalman filters
                      for( int i = 0; i != obj_map.size(); ++i )
                      {
                          auto filter = std::unique_ptr< Utils::Kalman2 >( new Utils::Kalman2() );
                          double x_mover = obj_map[i].first;
                          double y_mover = obj_map[i].second;
                          double vx = x_mover - obj_map_pre[i].first;
                          double vy = y_mover - obj_map_pre[i].second;
                          vx /= dt;
                          vy /= dt;

                          filter -> initialize( x_mover, y_mover, vx, vy, 0, 0 );
                          filters.push_back( std::move( filter )  );
                      }

                      //robot's Extended Kalman filter
                      state_now = state.player_state;
                      double vx = state_now.location.first - state_pre.location.first;
                      double vy = state_now.location.second - state_pre.location.second;
                      vx /= dt;
                      vy /= dt;
                      double speed = std::sqrt( vx*vx + vy*vy );

                      robot_filter -> initialize( state.player_state.location.first, state.player_state.location.second, speed, state.player_state.theta );
                      //now we can start the filtering procedure in the next cycle.
                      if_start = true;
                  }
                  else
                  {
                      //filtering procedure starts
                      //obstacles calculated from the movers: ( x, y, vx, vy ).
                      std::vector< UserStructs::StateXV > obstacles;
                      for( int i = 0; i != filters.size(); ++i )
                      {
                          double x_mover = obj_map[i].first;
                          double y_mover = obj_map[i].second;
                          double vx = x_mover - obj_map_pre[i].first;
                          double vy = y_mover - obj_map_pre[i].second;
                          vx /= dt;
                          vy /= dt;

                          //set measurement for a single kalman filter
                          filters[i] -> SetMeasure( x_mover, y_mover, vx, vy );
                          //dt will be used in the state prediction equation
                          filters[i] -> SetDt( dt );
                          double x, y;
                          //get updated position (x,y) and velocity (vx,vy)
                          filters[i] -> GetUpdate( x, y, vx, vy );

                          //log the filtered position and velocity
                          filter_f << std::setprecision(12) << Utils::GetTimeNow() << " "
                                   << i << " "
                                   << x << ' ' << y << ' '
                                   << vx << ' ' << vy << '\n';

                          //push in this moving obstacle
                          obstacles.push_back( UserStructs::StateXV( x_mover, y_mover, vx, vy ) );
                      }

                      //filtering for the robot
                      //position, speed, heading
                      double x, y, speed, theta;

                      //to get the speed measurement
                      double vx = state.player_state.location.first - state_pre.location.first;
                      double vy = state.player_state.location.second - state_pre.location.second;
                      vx /= dt;
                      vy /= dt;
                      double m_speed = std::sqrt( vx*vx + vy*vy );

                      //set measurement for the robot's filter
                      robot_filter -> SetMeasure( state.player_state.location.first, state.player_state.location.second, m_speed, state.player_state.theta );
                      //dt will be used in the state prediction equation
                      robot_filter -> SetDt( dt );
                      //steer angle will be used in the state prediction equation
                      robot_filter -> SetPhi( control );
                      //get the corrected robot's position, speed and heading
                      robot_filter -> GetUpdate( x, y, speed, theta );

                      //some times, the EKF goes crazy.
                      //in this case, we just re-initialize it with the measurement value
                      //so that the procedure will go on
                      if( speed < 0
                       || ( theta > 2 * M_PI || theta < -2 * M_PI )
                       || check_bbox_collision( x, y, bbox, 0 )
                        )
                      {
                          std::cout << "EKF error:" << '\n'
                                    << "x:" << x << ' '
                                    << "y:" << y << ' '
                                    << "speed:" << speed << ' '
                                    << "theta:" << theta << '\n';
                          speed = m_speed;
                          robot_filter -> initialize( state.player_state.location.first, state.player_state.location.second, speed, state.player_state.theta );
                      }

                      //log the filtered robot's position, speed and heading
                      filter_rf << std::setprecision(12) << Utils::GetTimeNow() << " "
                                << x << " " << y << " "
                                << speed << " " << theta << '\n';

                      //calculate the steer control from the motion planner
                      control = planner -> GetSteerMove2( speed, state.player_state, obstacles, bbox, t_predict, control );
                  }
              }
              /***log robot states start***/

              ofs << std::setprecision(12) << Utils::GetTimeNow() << " "
                  << state.player_state.location.first << " "
                  << state.player_state.location.second << " "
                  << state.player_state.theta << '\n';

              //usleep(16000);
              //to distinguish between the very first cycle and the following ones.
              if( count_cycle == 0 ){
                  ++ count_cycle;
              }

              //update the robot's and movers' state in the previous cycle,
              //they will be used in the next cycle.
              state_pre = state.player_state;
              obj_map_pre = obj_map;

              //update dt, it will be used in the kalman filters
              dt = Utils::GetTimeNow() - t1;
          }
      } catch(const sim::simulator::simulation_halt_exception& e)
      {
          std::cout<<e.what()<<"\n"<<e.final_state;
      }

  }

  bool SimuMain::check_bbox_collision( double x, double y, const sim::points_2d& bbox, double buffer )
  {
      bool xbound = x < bbox[0].first + buffer  || x > bbox[2].first -buffer;
      bool ybound = y < bbox[0].second + buffer || y > bbox[1].second -buffer;
      return xbound || ybound;
  }

}
